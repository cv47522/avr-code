const int stepperDir = 0;
const int stepperStep = 1;

void setup(){
  pinMode(stepperDir, OUTPUT);
  pinMode(stepperStep, OUTPUT);
}

void loop(){
  digitalWrite(stepperDir, HIGH); // CW or CCW direction
  //Move 20 steps(a full cycle)
  for(int i = 0;i < 3 * 8; i++){
  oneStep();
  }
  digitalWrite(stepperDir, LOW); // CW or CCW direction
  for(int i = 0;i < 3 * 8; i++){
  oneStep();
  }
}


void oneStep(){
  digitalWrite(1, HIGH);
  delayMicroseconds(100);
  digitalWrite(1, LOW);
  delayMicroseconds(100);
}
