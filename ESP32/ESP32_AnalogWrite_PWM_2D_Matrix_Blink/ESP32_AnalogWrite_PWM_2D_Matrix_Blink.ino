#include <analogWrite.h>
const int rows = 4;
const int columns = 5;
int ms = 200;

// Single Blink: not working motors w/ digital/analogWrite
//int LED = 13; // M3???: 13

// for Breadboard Controller
// Batch Blink: not working motors
// M3???: 13, M5: 26, M12(5)?: 5
// v3: Left-Hand, M15: IO4???, M8 (PCB): IO13???
int motorsArr[rows][columns] = {
//                           {19, 26, 27, 14, 23}, // thumb~little
//                           {5, 13, 33, 12, 22},
//                           {21, 17, 16, 18, 4}, // dw1, dw2, up1, dw3, up2
//                           {15, 25, 32, -1, -1} // back1~3
                             {26, 13, 12, -1, -1} 
                         };

// v3: Right-Hand, back1~3???
// int motorsArr[rows][columns] = {
//                               {23, 12, 33, 13, 19}, // thumb~little
////                               {22, 14, 27, 26, 5},
////                               {21, 18, 4, 32, 17},  // ??dwPalm1, dw2, up1, up2, dw3
////                               {16, 15, 25, -1, -1} // back1~3
//                             };
                          
void setup() {
  for (int i=0; i<rows; i++) {
    for (int j=0; j<columns; j++) {
      pinMode(motorsArr[i][j], OUTPUT);
    }
  }
  pinMode(13, OUTPUT);// important!!!
  turnAllOff();// important!!!
;}

void loop() {
// single blink
//  analogWrite(motorsArr[0][0], 150); // ON
//  analogWrite(motorsArr[0][1], 150); // ON
//  delay(ms);
//  analogWrite(motorsArr[0][0], 0); // OFF
//  analogWrite(motorsArr[0][1], 0); // OFF
//  delay(ms);
  
// point to whole hand
//  analogWrite(motorsArr[0][1], 100); // ON
//  delay(500);
//  analogWrite(motorsArr[0][1], 0); // OFF
//  delay(1500);
//  for (int i=0; i<rows; i++) {
//    for (int j=0; j<columns; j++) {
//      analogWrite(motorsArr[i][j], 100); // ON
//    }
//  }  
//  delay(1000);
//  turnAllOff();
//  delay(3000);
//  
// heartbeat
  for (int i=0; i<rows; i++) {
    for (int j=0; j<columns; j++) {
      analogWrite(motorsArr[i][j], 160); // ON
    }
  }
  delay(ms);
  
  for (int i=0; i<rows; i++) {
    for (int j=0; j<columns; j++) {
      analogWrite(motorsArr[i][j], 0); // OFF
    }
  }
  delay(ms);


// iteration
//      for (int i=0; i<rows; i++) {
//        for (int j=0; j<columns; j++) {
//          analogWrite(motorsArr[i][j], 120); // ON
//          delay(300);
////          turnAllOff();
//           analogWrite(motorsArr[i][j], 0); // OFF
////          delay(200);
//        }
//      }


// iteration with gradual growing intensity
//      for (int level=30; level<250; level++) {
//        for (int i=0; i<rows; i++) {
//          for (int j=0; j<columns; j++) {
//            analogWrite(motorsArr[i][j], level); // ON
//            delay(300);
//  //          turnAllOff();
//             analogWrite(motorsArr[i][j], 0); // OFF
//  //          delay(200);
//          }
//        }
//      }
////////////////
//    for (int i=0; i<rows; i++) {
//        for (int j=0; j<columns; j++) {
//          analogWrite(motorsArr[i][j], 150); // amplitude
//          Serial.print("Mode 1 turn ON pin: ");
//          Serial.println(motorsArr[i][j]);
//          delay(200);
//          analogWrite(motorsArr[i][j], 0); // OFF
//          Serial.print("OFF: ");
//          Serial.println(motorsArr[i][j]);
//          delay(200);
//        }
//       }
//       turnAllOff();
}

void turnAllOff() {
  for (int i=0; i<rows; i++) {
    for (int j=0; j<columns; j++) {
      analogWrite(motorsArr[i][j], 0); // OFF
    }
   }
}
