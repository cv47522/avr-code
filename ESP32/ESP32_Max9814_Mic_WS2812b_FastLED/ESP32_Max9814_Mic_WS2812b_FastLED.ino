//************
//Sound Level to light show sketch for the
//autogain microphone Amplifier from Adafruit on pin AO
//plus neopixel led functions (pin 6) mapped on to different sound levels to give music to light effects
//
//*************

//lines below set variables for neopixels

#include <FastLED.h>
#define MAX_POWER_MILLIAMPS 500
#define BRIGHTNESS          60
#define NUM_LEDS 8
#define DATA_PIN 12
CRGB leds[NUM_LEDS];


#define MIC 36

#define MAX_ANALOG_READ 4095 //ESP32



int lng = 100;//long delay
int sht = 50;//short delay

//lines below are for the microphone sampling from Adafruit autogain mic

const int sampleWindow = 50; // Sample window width in mS (50 mS = 20Hz)
unsigned int sample;

void setup()
{
   FastLED.addLeds<WS2812B, DATA_PIN, GRB>(leds, NUM_LEDS);  // GRB ordering is typical
//  FastLED.setMaxPowerInVoltsAndMilliamps(5, MAX_POWER_MILLIAMPS);
  FastLED.setBrightness(BRIGHTNESS);
  Serial.begin(115200);//set up for checking mic is working
}


void loop()
{
  //open void loop
  //first run the sound sampling
  unsigned long startMillis = millis(); // Start of sample window
  unsigned int peakToPeak = 0;   // peak-to-peak level

  unsigned int signalMax = 0;
  unsigned int signalMin = MAX_ANALOG_READ;

  // collect data for 50 mS
  while (millis() - startMillis < sampleWindow)
  {
    //open while loop
    sample = analogRead(MIC);
    if (sample < MAX_ANALOG_READ)  // toss out spurious readings
    {
      //open 1st if loop in while
      if (sample > signalMax)
      {
        //open 2nd if
        signalMax = sample;  // save just the max levels
      }//close 2nd if
      else if (sample < signalMin)
      {
        //open 3rd if
        signalMin = sample;  // save just the min levels
      }//close 3rd if
    }//close 1st if
  }//close while loop
  peakToPeak = signalMax - signalMin;  // max - min = peak-peak amplitude
  double volts = (peakToPeak * 3.3) / MAX_ANALOG_READ;  // convert to volts

//section below maps the signal from the microphone on to 12 options for LED effects

  int sound = (volts * 10);

  int soundLevel = map(sound, 1, 10, 0, 11);
  Serial.print("The volt level is  ");
  Serial.println(volts);//for debugging

//next section is a series of 12 (0-11) 'if' statements which trigger different patterns.
//it is a combination of a traditional VU style meter fill of the strip
// combined with randomised animated patterns to keep it interesting
  if (soundLevel == 0)
  {
    //open if 0. When there is silence a rainbow pattern runs

    for (int i=0; i<NUM_LEDS; i++) {
      leds[i] = CRGB::Black;
      FastLED.show();
    }

  }//close if 0 statement


  if (soundLevel == 1)
  {
    //open level 1 if statement which contains 4 randomised options

    int level1Color = random(1, 4);//choose random number 1 - 4

    if (level1Color == 1) //if random no 1 chosen light up pixels 1 and 2 red:
    {
      // this turns on pixel 1 100% red (range runs 0 - 255) and leaves green and blue off
      leds[0] = CRGB::Red;
      leds[1] = leds[0];
      for (int i=2; i<NUM_LEDS; i++) {
        leds[i] = CRGB::Black;
      }
      FastLED.show();
      delay(lng);
    }//close red random no. 1

    else if (level1Color == 2) //if random no 2 choses show green
    {
      leds[0] = CRGB::Green;
      leds[1] = leds[0];
      for (int i=2; i<NUM_LEDS; i++) {
        leds[i] = CRGB::Black;
      }
      FastLED.show();
      delay(lng);
    }//close random no. 2 green

    else if (level1Color == 3) //run blue
    {
      leds[0] = CRGB::Blue;
      leds[1] = leds[0];
      for (int i=2; i<NUM_LEDS; i++) {
        leds[i] = CRGB::Black;
      }
      FastLED.show();
      delay(lng);
    }//close blue

    else if (level1Color == 4) //run yellow
    {
      leds[0] = CRGB::Yellow;
      leds[1] = leds[0];
      for (int i=2; i<NUM_LEDS; i++) {
        leds[i] = CRGB::Black;
      }
      FastLED.show();
      delay(lng);
    }//close yellow
  }//end of if sound level 1 options

  if (soundLevel == 2)
  {
    //open level 2

    int level2Color = random(1, 5);//choose one of 5 options if sound level 2

    if (level2Color == 1) //run red mix
    {
      leds[0].setRGB(255, 0, 0);
      leds[1].setRGB(0, 0, 255);
      leds[2].setRGB(255, 0, 0);
      for (int i=2; i<NUM_LEDS; i++) {
        leds[i] = CRGB::Black;
      }
      FastLED.show();
      delay(lng);
    }//close option 1 red

    else if (level2Color == 2) //run green mix
    {
      //open option 2
      leds[0].setRGB(0, 206, 209);
      leds[1] = leds[0];
      leds[2] = leds[0];
      for (int i=2; i<NUM_LEDS; i++) {
        leds[i] = CRGB::Black;
      }
      FastLED.show();
      delay(lng);
    }//close green

    else if (level2Color == 3) //run blue mix
    {
      //open option 3
      leds[0].setRGB(0, 0, 255);
      leds[1].setRGB(255, 0, 0);
      leds[2].setRGB(0, 0, 255);
      for (int i=2; i<NUM_LEDS; i++) {
        leds[i] = CRGB::Black;
      }
      FastLED.show();
      delay(lng);
    }//close option 3 blue

    else if (level2Color == 4) //run yellow
    {
      //open option 4
      leds[0].setRGB(255, 2255, 0);
      leds[1] = leds[0];
      leds[2] = leds[0];
      for (int i=2; i<NUM_LEDS; i++) {
        leds[i] = CRGB::Black;
      }
      FastLED.show();
      delay(lng);
    }//close yellow

    else if (level2Color == 5)//for a bit of variation 1 in 5 of level 2 will show a pattern across whole strip:
    {
      //open if 5
      leds[0].setRGB(200, 75, 109);  
      leds[1].setRGB(252, 203, 198);  
      leds[2].setRGB(255, 216, 209);  
      leds[3].setRGB(253, 215, 130);  
      leds[4].setRGB(181, 198, 130);  
      leds[5].setRGB(141, 189, 193);  
      leds[6].setRGB(177, 217, 242);  
      leds[7].setRGB(100, 165, 187);  
      FastLED.show();
      delay(lng);

      leds[1].setRGB(200, 75, 109);  
      leds[2].setRGB(252, 203, 198);  
      leds[3].setRGB(255, 216, 209);  
      leds[4].setRGB(253, 215, 130);  
      leds[5].setRGB(181, 198, 130);  
      leds[6].setRGB(141, 189, 193);  
      leds[7].setRGB(177, 217, 242);   
      leds[0].setRGB(100, 165, 187);  

      FastLED.show();
      delay(lng);

    }//close of option 5
  }//close level 2

  if (soundLevel == 3)
  {
    //open if sound level 3
    int level3Color = random(1, 5);

    if (level3Color == 1) //run red
    {
      //open option 1  
      leds[0].setRGB(255, 0, 0);
      leds[1].setRGB(0, 255, 0); 
      leds[2].setRGB(255, 0, 0);
      leds[1].setRGB(0, 255, 0); 
      for (int i=4; i<NUM_LEDS; i++) {
        leds[i] = CRGB::Black;
      } 
      FastLED.show();
      delay(lng);
    }//close option 1 red

    else if (level3Color == 2) //run green
    {
      //open option 2

      leds[0].setRGB(245, 116, 97);  
      leds[1].setRGB(169, 221, 20);  
      leds[2].setRGB(245, 116, 97);  
      leds[3].setRGB(169, 221, 20);  
      for (int i=4; i<NUM_LEDS; i++) {
        leds[i] = CRGB::Black;
      } 
      FastLED.show();
      delay(lng);
    }//close option 2green

    else if (level3Color == 3) //run blue
    {
      //open option 3

      leds[0].setRGB(169, 221, 199);  
      leds[1].setRGB(245, 116, 97);  
      leds[2].setRGB(169, 221, 199);  
      leds[3].setRGB(245, 116, 97);  
      for (int i=4; i<NUM_LEDS; i++) {
        leds[i] = CRGB::Black;
      }  
      FastLED.show();
      delay(lng);
    }//close option 3 blue

    else if (level3Color == 4) //run yellow
    {
      //open option 4
      for (int i=0; i<4; i++) {
        leds[i].setRGB(255, 255, 0);  
      }
      for (int i=4; i<NUM_LEDS; i++) {
        leds[i] = CRGB::Black;
      }   
      FastLED.show();
      delay(lng);
    }//close option 4 yellow

    else if (level3Color == 5)
    {
      //open option 5
      for (int i=0; i<NUM_LEDS; i+2) {
        leds[i].setRGB(255, 255, 255);   
      }
      for (int i=1; i<NUM_LEDS; i+2) {
        leds[i].setRGB(255, 105, 180);  
      }  
      FastLED.show();
      delay(sht);
    }//close of option 5
  }//close level 3


  if (soundLevel == 4)
  {
    //open if sound level 4
    int level4Color = random(1, 5);

    if (level4Color == 1) //run red
    {
      //open option 1

      leds[0].setRGB(255, 0, 0);  
      leds[1].setRGB(0, 0, 255);  
      leds[2].setRGB(255, 0, 0);  
      leds[3].setRGB(0, 0, 255);  
      leds[4].setRGB(255, 0, 0);  
      for (int i=5; i<NUM_LEDS; i++) {
        leds[i] = CRGB::Black;
      }   
      FastLED.show();
      delay(lng);
    }//close red

    else if (level4Color == 2) //run green
    {
      //open option 2
      for (int i=0; i<5; i++) {
        leds[i].setRGB(0, 255, 0);  
      }   
      for (int i=5; i<NUM_LEDS; i++) {
        leds[i] = CRGB::Black;
      }  
      FastLED.show();
      delay(lng);
    }//close green

    else if (level4Color == 3) //run blue
    {
      //open option 3
      for (int i=0; i<5; i++) {
        leds[i].setRGB(0, 0, 255);   
      }   
      for (int i=5; i<NUM_LEDS; i++) {
        leds[i] = CRGB::Black;
      }    
      FastLED.show();
      delay(lng);
    }//close blue

    else if (level4Color == 4) //run yellow
    {
      //open option 4
      for (int i=0; i<5; i++) {
        leds[i].setRGB(255, 255, 0); 
      }   
      for (int i=5; i<NUM_LEDS; i++) {
        leds[i] = CRGB::Black;
      }   
      FastLED.show();
      delay(lng);
    }//close yellow

    else if (level4Color == 5)
    {
      ////open option 5
      leds[0].setRGB(255, 01, 165);  
      leds[1].setRGB(255, 187, 218);  
      leds[2].setRGB(228, 194, 191);  
      leds[3].setRGB(153, 87, 205);  
      leds[4].setRGB(176, 284, 218);  
      leds[5].setRGB(67, 142, 200);  
      leds[6].setRGB(107, 167, 214);  
      leds[7].setRGB(168, 204, 232);   
      FastLED.show();
      delay(lng);
    }//close option 5

  }//close if sound level 4

  else if (soundLevel == 5)
  {
    //open if sound level 5


    int level5Color = random(1, 6);

    if (level5Color == 1) //run red
    {
      //open option 1

      leds[0].setRGB(255, 0, 0);  
      leds[1].setRGB(255, 255, 255);  
      leds[2].setRGB(0, 0, 255);  
      leds[3].setRGB(255, 0, 0);  
      leds[4].setRGB(255, 255, 255);  
      leds[5].setRGB(0, 0, 255);  
      leds[6].setRGB(0, 0, 0);  
      leds[7].setRGB(0, 0, 0);   
      FastLED.show();
      delay(lng);
    }//close option 1 red

    else if (level5Color == 2) //run green
    {
      //open option 2
      for (int i=0; i<6; i++) {
        leds[i].setRGB(0, 255, 0);  
      }   
      for (int i=6; i<NUM_LEDS; i++) {
        leds[i] = CRGB::Black;
      } 
      FastLED.show();
      delay(lng);
    }//close option 2 green

    else if (level5Color == 3) //run blue
    {
      //open option 3
      for (int i=0; i<6; i++) {
        leds[i].setRGB(0, 0, 255);   
      }   
      for (int i=6; i<NUM_LEDS; i++) {
        leds[i] = CRGB::Black;
      }
      FastLED.show();
      delay(lng);
    }//close option 3 blue

    else if (level5Color == 4) //run yellow
    {
      //open option 4
      for (int i=0; i<6; i++) {
        leds[i].setRGB(255, 255, 0);   
      }   
      for (int i=6; i<NUM_LEDS; i++) {
        leds[i] = CRGB::Black;
      }
      FastLED.show();
      delay(lng);
    }//close yellow

    else if (level5Color == 5)
    {
      //open option 5
      
      leds[0].setRGB(255, 0, 0);  
      leds[1].setRGB(0, 255, 0);  
      leds[2].setRGB(0, 0, 255);  
      leds[3].setRGB(255, 0, 0);  
      leds[4].setRGB(0, 255, 0);  
      leds[5].setRGB(0, 0, 255);
      leds[6].setRGB(255, 0, 0);  
      leds[7].setRGB(0, 255, 0);  
      FastLED.show();
      delay(lng);
    }//close option 5

    else if (level5Color == 6)
    {
      //open option 6

      colorWipe(CRGB(255, 0, 255), 50); // magenta
      colorWipe(CRGB(0, 255, 0), 50); // green
      FastLED.show();
    }//close option 6
  }//close if sound level 5


  else if (soundLevel == 6)
  {
    //open if soundlevel 6

    int level6Color = random(1, 6);

    if (level6Color == 1) //run red
    {
      //open option 1

      leds[0].setRGB(255, 0, 0);  
      leds[1].setRGB(255, 255, 255);  
      leds[2].setRGB(0, 0, 255);  
      leds[3].setRGB(255, 0, 0);  
      leds[4].setRGB(255, 255, 255);  
      leds[5].setRGB(0, 0, 255);  
      leds[6].setRGB(255, 0, 0);  
      leds[7].setRGB(0, 0, 0);  
      FastLED.show();
      delay(lng);
    }//close option 1red

    else if (level6Color == 2) //run green
    {
      //open option 2
      for(int i=0; i<NUM_LEDS;i++){
        leds[i] = CRGB::Green;
      }  
      FastLED.show();
      delay(lng);
    }//close option 2 green

    else if (level6Color == 3) //run blue
    {
      //open option 3
      for(int i=0; i<NUM_LEDS;i++){
        leds[i] = CRGB::Blue;
      }  
      FastLED.show();
      delay(lng);
    }//close option 3 blue

    else if (level6Color == 4) //run yellow
    {
      //open option 4

      leds[0].setRGB(148, 0, 211);  
      leds[1].setRGB(75, 0, 130);  
      leds[2].setRGB(0, 0, 255);  
      leds[3].setRGB(0, 255, 0);  
      leds[4].setRGB(255, 255, 0);  
      leds[5].setRGB(255, 127, 0);  
      leds[6].setRGB(255, 0, 0);  
      leds[7].setRGB(0, 0, 0);  
      FastLED.show();
      delay(sht);
    }//close yellow

    else if (level6Color == 5)
    {
      //open option 5
      colorWipe(CRGB(0, 0, 255), 50); // Blue
      colorWipe(CRGB(255, 255, 0), 50); // yellow
      FastLED.show();
    }//close option 5

//    else if (level6Color == 6)
//    {
//      //open option6
//
//      theaterChase(CRGB(200, 0, 0), 50); // Red
//      FastLED.show();
//      delay(lng);
//    }//close option 6
  }//close if sound level 6

  else if (soundLevel == 7)
  {
    //open if sound level 7
    int level7Color = random(1, 7);

    if (level7Color == 1) //run red
    {
      //open option 1
      for(int i=0; i<NUM_LEDS;i++){
        leds[i] = CRGB::Red;
      }  
      FastLED.show();
      delay(lng);
    }//close option 1 red

    else if (level7Color == 2) //run green
    {
      //open option 2
      for(int i=0; i<NUM_LEDS;i++){
        leds[i] = CRGB::Green;
      }  
      FastLED.show();
      delay(lng);
    }//close option 2 green

    else if (level7Color == 3) //run blue
    {
      //open option 3
      for(int i=0; i<NUM_LEDS;i++){
        leds[i] = CRGB::Blue;
      } 
      FastLED.show();
      delay(lng);
    }//close option 3 blue

    else if (level7Color == 4) //run yellow
    {
      //open option 4
      for(int i=0; i<NUM_LEDS;i++){
        leds[i] = CRGB::Yellow;
      } 
      FastLED.show();
      delay(lng);
    }//close option 4 yellow

    else if (level7Color == 5)
    {
      //open option 5
      colorWipe(CRGB(255, 20, 147), 50); // pink
      colorWipe(CRGB(0, 206, 209), 50); // turquoise
      FastLED.show();
      delay(lng);
    }//close option 5

//    else if (level7Color == 6)
//    {
//      //open option 6
//
//      theaterChase(CRGB(255, 20, 100), 50); // Red
//      FastLED.show();
//      delay(sht);
//    }//close option 6

    else if (level7Color == 7)
    {
      //open option 7
      leds[0].setRGB(0, 70, 70);  
      leds[1].setRGB(0, 100, 0);  
      leds[2].setRGB(255, 0, 70);  
      leds[3].setRGB(50, 0, 150);  
      leds[4].setRGB(0, 70, 70);  
      leds[5].setRGB(0, 100, 0);  
      leds[6].setRGB(255, 0, 70);  
      leds[7].setRGB(50, 0, 150); 
      FastLED.show();
      delay(sht);
    }//close option 7
  }//close if sound level 7

  else if (soundLevel == 8)
  {
    //open if sound level 8

    int level8Color = random(1, 8);

    if (level8Color == 1) //run red
    {
      //open option 1
      for(int i=0; i<NUM_LEDS;i++){
        leds[i] = CRGB::Red;
      } 
      FastLED.show();
      delay(lng);
    }//close option 1 red

    else if (level8Color == 2) //run green/blue
    {
      //open option 2
      for(int i=0; i<NUM_LEDS;i+2){
        leds[i].setRGB(0, 255, 0); 
      } 
      for(int i=1; i<NUM_LEDS;i+2){
        leds[i].setRGB(0, 0, 255);
      }  
      FastLED.show();
      delay(lng);
    }//close option 2 green/blue

    else if (level8Color == 3) //run turquoise / blue
    {
      //open option 3
      for(int i=0; i<NUM_LEDS;i+2){
        leds[i].setRGB(0, 206, 255);
      } 
      for(int i=1; i<NUM_LEDS;i+2){
        leds[i].setRGB(0, 0, 255);
      }  
      FastLED.show();
      delay(lng);
    }//close option 3 blue

    else if (level8Color == 4) //run yellow
    {
      //open option 4
      for(int i=0; i<NUM_LEDS;i++){
        leds[i] = CRGB::Yellow;
      } 
      FastLED.show();
      delay(lng);
    }//close option 4 yellow

    else if (level8Color == 5)
    {
      //open option 5
      colorWipe(CRGB(255, 20, 147), 20); // pink
      colorWipe(CRGB(0, 206, 209), 20); // turquoise
      FastLED.show();
    }//close option 5

//    else if (level8Color == 6)
//    {
//      //open option 6
//
//      theaterChase(CRGB(0, 206, 209), 50); // Red
//      FastLED.show();
//      delay(sht);
//    }//close option 6

    else if (level8Color == 7)
    {
      //open option 7
      leds[0].setRGB(0, 70, 70);  
      leds[1].setRGB(0, 100, 0);  
      leds[2].setRGB(255, 0, 70);  
      leds[3].setRGB(50, 0, 150);  
      leds[4].setRGB(0, 70, 70);  
      leds[5].setRGB(0, 100, 0);  
      leds[6].setRGB(255, 0, 70);  
      leds[7].setRGB(50, 0, 150);  
      FastLED.show();
      delay(lng);

      leds[0].setRGB(0, 255, 255);  
      leds[1].setRGB(0, 255, 255);  
      leds[2].setRGB(0, 0, 0);  
      leds[3].setRGB(255, 255, 0);  
      leds[4].setRGB(255, 255, 0);  
      leds[5].setRGB(0, 0, 0);  
      leds[6].setRGB(0, 255, 255);  
      leds[7].setRGB(0, 255, 255);  
      FastLED.show();
      delay(lng);


    }//close option 7

  }//close if sound level 8

  else if (soundLevel == 9)
  {
    //open if sound level 9

    int level9Color = random(1, 8);

    if (level9Color == 1) //run red
    {
      //open option 1
      for(int i=0; i<NUM_LEDS;i++){
        leds[i] = CRGB::Red;
      } 
      FastLED.show();
      delay(lng);
    }//close option 1 red

    else if (level9Color == 2) //run green
    {
      //open option 2
      for(int i=0; i<NUM_LEDS;i+2){
        leds[i].setRGB(0, 255, 0);
      }  
      for(int i=1; i<NUM_LEDS;i+2){
        leds[i].setRGB(0, 0, 255);
      }  
      FastLED.show();
      delay(lng);
    }//close option 2 green

    else if (level9Color == 3) //run blue
    {
      //open option 3
      for(int i=0; i<NUM_LEDS;i+2){
        leds[i].setRGB(255, 0, 255);
      }  
      for(int i=1; i<NUM_LEDS;i+2){
        leds[i].setRGB(0, 0, 255);
      }  
      FastLED.show();
      delay(lng);
    }//close option 3blue

    else if (level9Color == 4) //run yellow
    {
      //open option 4
      for(int i=0; i<NUM_LEDS;i++){
        leds[i] = CRGB::Yellow;
      }   
      FastLED.show();
      delay(lng);
    }//close option 4 yellow

    else if (level9Color == 5)
    {
      //open option 5
      colorWipe(CRGB(255, 255, 255), 60); // white
      colorWipe(CRGB(0, 206, 209), 20); // turquoise
      FastLED.show();
    }//close option 5

//    else if (level9Color == 6)
//    {
//      //open option 6
//
//      theaterChase(CRGB(50, 190, 209), 50); // turquise
//      FastLED.show();
//      delay(lng);
//    }//close option 6

    else if (level9Color == 7)
    {
      //open option 7
      leds[0].setRGB(0, 70, 70);  
      leds[1].setRGB(0, 100, 0);  
      leds[2].setRGB(255, 0, 70);  
      leds[3].setRGB(50, 0, 150);  
      leds[4].setRGB(0, 70, 70);  
      leds[5].setRGB(0, 100, 0);  
      leds[6].setRGB(255, 0, 70);  
      leds[7].setRGB(50, 0, 150); 

      FastLED.show();
      delay(lng);

      leds[0].setRGB(0, 255, 255);  
      leds[1].setRGB(0, 255, 255);  
      leds[2].setRGB(0, 0, 0);  
      leds[3].setRGB(255, 255, 0);  
      leds[4].setRGB(255, 255, 0);  
      leds[5].setRGB(0, 0, 0);  
      leds[6].setRGB(0, 255, 255);  
      leds[7].setRGB(0, 255, 255);
      FastLED.show();
      delay(lng);

      leds[0].setRGB(255, 50, 50);  
      leds[1].setRGB(0, 255, 0);  
      leds[2].setRGB(255, 50, 50);  
      leds[3].setRGB(255, 0, 0);  
      leds[4].setRGB(255, 50, 50);  
      leds[5].setRGB(0, 0, 255);  
      leds[6].setRGB(255, 50, 50);  
      leds[7].setRGB(0, 255, 0);  
      FastLED.show();

      delay(lng);


    }//close option 7

    else if (level9Color == 8)
    {
      //open option 8
      for(int i=0; i<NUM_LEDS;i+2){
        leds[i] = CRGB::White;
      }
      for(int i=1; i<NUM_LEDS;i+2){
        leds[i] = CRGB::Black;
      } 
      FastLED.show();
      delay (lng);
      
      for(int i=0; i<NUM_LEDS;i+2){
        leds[i] = CRGB::Black;
      }
      for(int i=1; i<NUM_LEDS;i+2){
        leds[i] = CRGB::White;
      } 
      FastLED.show();
      delay(lng);
    }//close option 9
  }//close if sound level 9

  else if (soundLevel == 10)

  {
    //open if sound Level 10

    int level10Color = random(1, 8);

    if (level10Color == 1) //run red
    {
      //open option 1

      for(int i=0; i<NUM_LEDS;i++){
        leds[i] = CRGB::Red;
      } 
      FastLED.show();
      delay(lng);
    }//close option 1 red

    else if (level10Color == 2) //run green
    {
      //open option 2
      for(int i=0; i<NUM_LEDS;i+2){
        leds[i] = CRGB::Green;
      }
      for(int i=1; i<NUM_LEDS;i+2){
        leds[i] = CRGB::Blue;
      } 
      FastLED.show();
      delay(lng);
    }//close option 2 green

    else if (level10Color == 3) //run blue
    {
      //open option 3
      for(int i=0; i<NUM_LEDS;i+2){
        leds[i].setRGB(0, 206, 255);
      }
      for(int i=1; i<NUM_LEDS;i+2){
        leds[i] = CRGB::Blue;
      }
      FastLED.show();
      delay(lng);
    }//close option 3 blue

    else if (level10Color == 4) //run yellow
    {
      //open option 4

      for(int i=0; i<NUM_LEDS;i++){
        leds[i] = CRGB::Yellow;
      } 
      FastLED.show();
      delay(lng);
    }//close option 4 yellow

    else if (level10Color == 5)
    {
      //open option 5

      colorWipe(CRGB(200, 40, 147), 50); // pink
      colorWipe(CRGB(0, 206, 209), 20); // turquoise
      FastLED.show();
      delay(sht);
    }//close option 5

//    else if (level10Color == 6)
//    {
//      //open option 6
//      theaterChase(CRGB(0, 206, 209), 50);
//      FastLED.show();
//      delay(sht);
//    }//close option 6

    else if (level10Color == 7)
    {
      //open option 7
      leds[0].setRGB(0, 70, 70);  
      leds[1].setRGB(0, 100, 0);  
      leds[2].setRGB(255, 0, 70);  
      leds[3].setRGB(50, 0, 150);  
      leds[4].setRGB(0, 70, 70);  
      leds[5].setRGB(0, 100, 0);  
      leds[6].setRGB(255, 0, 70);  
      leds[7].setRGB(50, 0, 150);

      FastLED.show();
      delay(lng);

      leds[0].setRGB(0, 255, 255);  
      leds[1].setRGB(0, 255, 255);  
      leds[2].setRGB(0, 0, 0);  
      leds[3].setRGB(255, 255, 0);  
      leds[4].setRGB(255, 255, 0);  
      leds[5].setRGB(0, 0, 0);  
      leds[6].setRGB(0, 255, 255);  
      leds[7].setRGB(0, 255, 255);
      FastLED.show();
      delay(lng);

      for(int i=0; i<NUM_LEDS;i++){
        leds[i] = CRGB::White;
      } 
      FastLED.show();
      delay(sht);

    }//close option 7

  }//close if sound level 10


  else if (soundLevel == 11)

  {
    //open if sound Level 11

    int level11Color = random(1, 8);

    if (level11Color == 1) //run red
    {
      //open option 1

      for(int i=0; i<NUM_LEDS;i++){
        leds[i] = CRGB::Red;
      }  
      FastLED.show();
      delay(lng);
    }//close option 1 red

    else if (level11Color == 2) //run green
    {
      //open option 2
      for(int i=0; i<NUM_LEDS;i+2){
        leds[i] = CRGB::Green;
      } 
      for(int i=1; i<NUM_LEDS;i+2){
        leds[i] = CRGB::Blue;
      } 
      FastLED.show();
      delay(lng);
    }//close option 2  green

    else if (level11Color == 3) //run blue
    {
      //open option 3
      for(int i=0; i<NUM_LEDS;i+2){
        leds[i].setRGB(0, 206, 255);
      } 
      for(int i=1; i<NUM_LEDS;i+2){
        leds[i] = CRGB::Blue;
      }
      FastLED.show();
      delay(lng);
    }//close option 3 blue

    else if (level11Color == 4) //run yellow
    {
      //open option 4
      for(int i=0; i<NUM_LEDS;i++){
        leds[i] = CRGB::Yellow;
      }
      FastLED.show();
      delay(lng);
    }//close option 4 yellow

    else if (level11Color == 5)
    {
      //open option 5

      colorWipe(CRGB(0, 40, 255), 50); // pink
      colorWipe((0, 209, 206), 20); // turquoise
      FastLED.show();
      delay(sht);
    }//close option 5

//    else if (level11Color == 6) //open option 6
//    {
//      //open option 6
//      theaterChase(CRGB(0, 206, 109), 50);
//      FastLED.show();
//      delay(sht);
//    }//close option 6

    else if (level11Color == 7)//open option 7
    {
      //open option 7
      leds[0].setRGB(0, 70, 70);  
      leds[1].setRGB(0, 100, 0);  
      leds[2].setRGB(255, 0, 70);  
      leds[3].setRGB(50, 0, 150);  
      leds[4].setRGB(0, 70, 70);  
      leds[5].setRGB(0, 100, 0);  
      leds[6].setRGB(255, 0, 70);  
      leds[7].setRGB(50, 0, 150);
      FastLED.show();
      delay(lng);

      leds[0].setRGB(0, 255, 255);  
      leds[1].setRGB(0, 255, 255);  
      leds[2].setRGB(0, 0, 0);  
      leds[3].setRGB(255, 255, 0);  
      leds[4].setRGB(255, 255, 0);  
      leds[5].setRGB(0, 0, 0);  
      leds[6].setRGB(0, 255, 255);  
      leds[7].setRGB(0, 255, 255); 
      FastLED.show();
      delay(lng);

      for(int i=0; i<NUM_LEDS;i++){
        leds[i] = CRGB::Yellow;
      }
      FastLED.show();
      delay(lng);

      leds[0].setRGB(0, 255, 255);  
      leds[1].setRGB(0, 255, 255);  
      leds[2].setRGB(0, 0, 0);  
      leds[3].setRGB(255, 255, 0);  
      leds[4].setRGB(255, 255, 0);  
      leds[5].setRGB(0, 0, 0);  
      leds[6].setRGB(0, 255, 255);  
      leds[7].setRGB(0, 255, 255);
      FastLED.show();
      delay(lng);

    }//close option 7

  }//close if sound level 11

}//close void loop()

//neopixel functions below



void colorWipe(CRGB c, uint8_t wait)
{
  //open colowipe
  for(uint16_t i = 0; i < NUM_LEDS; i++)
  {
    leds[i] = c;
    FastLED.show();
    delay(wait);
  }
}//close colorWipe function

void theaterChase(CRGB c, uint8_t wait)
{
  //open theaterchase function
  for (int j = 0; j < 3; j++) //do 3 cycles of chasing
  {
    for (int q = 0; q < 3; q++)
    {
      for (int i = 0; i < NUM_LEDS; i = i + 3)
      {
        leds[i+q] = c;  //turn every third pixel on
      }
      FastLED.show();

      delay(wait);

      for (int i = 0; i < NUM_LEDS; i = i + 3)
      {
        leds[i+q].setRGB(0,0,0);      //turn every third pixel off
      }
    }
  }
}//close theater chase function

//void rainbowCycle(uint8_t wait)  //open rainbow function
//{
//  uint16_t i, j;
//
//  for(j = 0; j < 256; j++) // 1 cycles of all colors on wheel
//  {
//    for(i = 0; i < NUM_LEDS; i++)
//    {
//      leds[i].setRGB(Wheel(((i * 256 / NUM_LEDS) + j) & 255));
//    }
//    FastLED.show();
//    delay(wait);
//  }
//}//close rainbow function
////you need to included the code below for the neopixel functions that use the 'wheel' code:
//uint32_t Wheel(byte WheelPos)
//{
//  WheelPos = 255 - WheelPos;
//  if(WheelPos < 85)
//  {
//    return CRGB(255 - WheelPos * 3, 0, WheelPos * 3);
//  }
//  if(WheelPos < 170)
//  {
//    WheelPos -= 85;
//    return CRGB(0, WheelPos * 3, 255 - WheelPos * 3);
//  }
//  WheelPos -= 170;
//  return CRGB(WheelPos * 3, 255 - WheelPos * 3, 0);
//}

//end
