#include <FastLED.h>

// How many leds in your strip?
#define NUM_LEDS 60

// For led chips like WS2812, which have a data line, ground, and power, you just
// need to define DATA_PIN.  For led chipsets that are SPI based (four wires - data, clock,
// ground, and power), like the LPD8806 define both DATA_PIN and CLOCK_PIN
// Clock pin only needed for SPI based chipsets when not using hardware SPI
#define DATA_PIN 17

// Define the array of leds
CRGB leds[NUM_LEDS];

void setup() { 
  // limit my draw to 1A at 5v of power draw
  FastLED.setMaxPowerInVoltsAndMilliamps(5,1000); 
  FastLED.addLeds<WS2812B, DATA_PIN, GRB>(leds, NUM_LEDS);  // GRB ordering is typical
//  FastLED.clear();
}

void loop() {
  singleBlink();
  moveAnLed();
}

void moveAnLed() {
  for(int i = 0; i < NUM_LEDS; i++) {
    // turn ON
    leds[i] = CRGB::DarkCyan;
    FastLED.show();
    delay(30); // 24fps frame rate
    // turn OFF
    leds[i] = CRGB::Black;
    FastLED.show();
    delay(30);
  }
}

void singleBlink() { 
  // Turn the LED on, then pause
  // members of classes ( :: ), pointer members ( -> )
  // Color types: http://fastled.io/docs/3.1/struct_c_r_g_b.html#details
//  leds[4] = CRGB::Blue;
  leds[4] = CRGB(0, 255, 0);
  FastLED.show();
  delay(500);
  // Now turn the LED off, then pause
  leds[4] = CRGB::Black;
  FastLED.show();
  delay(500);
}
