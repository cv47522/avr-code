int sensorPin = 13;    // select the input pin for the potentiometer
int ledPin = 4;      // select the pin for the LED
int sensorValue = 0;  // variable to store the value coming from the sensor

void setup() {
  // declare the ledPin as an OUTPUT:
  pinMode(ledPin, OUTPUT);
  Serial.begin(115200);
}

void loop() {
  // read the value from the sensor:
  sensorValue = digitalRead(sensorPin);
  Serial.println(sensorValue);
  delay(100);
}
