/*********
  Rui Santos
  Complete project details at http://randomnerdtutorials.com  
*********/

// Load Wi-Fi library
#include <WiFi.h>

// Replace with your network credentials
//const char* ssid     = "aalto open";
const char* ssid     = "TP-LINK_CBDE";
const char* password = "50268238";
 
// Set web server port number to 80
WiFiServer server(80);

// Variable to store the HTTP request
String header;

// Auxiliar variables to store the current output state
String output26State = "off";
String output27State = "off";

// Assign output variables to GPIO pins
const int output26 = 32;
const int output27 = 33;



void setup() {
  Serial.begin(115200);
  // Initialize the output variables as outputs
  pinMode(output26, OUTPUT);
  pinMode(output27, OUTPUT);
  // Set outputs to LOW
  digitalWrite(output26, LOW);
  digitalWrite(output27, LOW);

  // Connect to Wi-Fi network with SSID and password
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  // Print local IP address and start web server
  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  server.begin();
}

void loop(){
  WiFiClient client = server.available();   // Listen for incoming clients

  if (client) {                             // If a new client connects,
    Serial.println("New Client.");          // print a message out in the serial port
    String currentLine = "";                // make a String to hold incoming data from the client
    while (client.connected()) {            // loop while the client's connected
      if (client.available()) {             // if there's bytes to read from the client,
        char c = client.read();             // read a byte, then
        Serial.write(c);                    // print it out the serial monitor
        header += c;
        if (c == '\n') {                    // if the byte is a newline character
          // if the current line is blank, you got two newline characters in a row.
          // that's the end of the client HTTP request, so send a response:
          if (currentLine.length() == 0) {
            // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
            // and a content-type so the client knows what's coming, then a blank line:
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println("Connection: close");
            client.println();
            
            // turns the GPIOs on and off
            if (header.indexOf("GET /" + String(output26) + "/on") >= 0) {
              turnOn(output26, output26State);
              output26State = "on";
            } else if (header.indexOf("GET /" + String(output26) + "/off") >= 0) {
              turnOff(output26, output26State);
              output26State = "off";
            } else if (header.indexOf("GET /" + String(output27) + "/on") >= 0) {
              turnOn(output27, output27State);
              output27State = "on";
            } else if (header.indexOf("GET /" + String(output27) + "/off") >= 0) {
              turnOff(output27, output27State);
              output27State = "off";
            }
            
            displyWebPage(client);
            
            displayPinState(client,output26, output26State);
                      
            displayPinState(client,output27, output27State);
            
            client.println("</body></html>");
            
            // The HTTP response ends with another blank line
            client.println();
            // Break out of the while loop
            break;
          } else { // if you got a newline, then clear currentLine
            currentLine = "";
          }
        } else if (c != '\r') {  // if you got anything else but a carriage return character,
          currentLine += c;      // add it to the end of the currentLine
        }
      }
    }
    // Clear the header variable
    header = "";
    // Close the connection
    client.stop();
    Serial.println("Client disconnected.");
    Serial.println("");
  }
}

void displyWebPage(WiFiClient c){
  // Display the HTML web page
  c.println("<!DOCTYPE html><html>");
  c.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
  c.println("<link rel=\"icon\" href=\"data:,\">");
  // CSS to style the on/off buttons 
  // Feel free to change the background-color and font-size attributes to fit your preferences
  c.println("<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}");
  c.println(".button { background-color: #4CAF50; border: none; color: white; padding: 16px 40px;");
  c.println("text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}");
  c.println(".button2 {background-color: #555555;}</style></head>");
  
  // Web Page Heading
  c.println("<body><h1>ESP32 Web Server</h1>");
}

void displayPinState(WiFiClient c, int pin, String pinState){
  // Display current state, and ON/OFF buttons for GPIO 26  
  c.println("<p>GPIO " + String(pin) + " - State " + pinState + "</p>");
  // If the output26State is off, it displays the ON button       
  if (pinState=="off") {
    c.println("<p><a href=\"/"+ String(pin) + "/on\"><button class=\"button\">ON</button></a></p>");
  } else {
    c.println("<p><a href=\"/"+ String(pin) + "/off\"><button class=\"button button2\">OFF</button></a></p>");
  }
}

void turnOn(int pin, String pinState){
  Serial.println("GPIO " + String(pin) + " on");
 // pinState = "on";
  digitalWrite(pin, HIGH);
}

void turnOff(int pin, String pinState){
  Serial.println("GPIO " + String(pin) + " off");
  //pinState = "off";
  digitalWrite(pin, LOW);
}
