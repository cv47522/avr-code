#include <analogWrite.h>
#define LED 27                                                                  

void setup() {
  // put your setup code here, to run once:
  pinMode(LED, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  analogWrite(LED, 240); // amplitude
  delay(1000);
  analogWrite(LED, 30); // OFF
  delay(1000);

}
