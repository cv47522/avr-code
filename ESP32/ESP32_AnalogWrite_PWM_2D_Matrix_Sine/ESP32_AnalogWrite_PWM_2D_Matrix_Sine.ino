#include <analogWrite.h>
const byte rows = 3;
const byte columns = 5;

// 2D array (row, column): store relative location
int motors[rows][columns] = {
                              {25, 32, 33, 34, 21},
                              {26, 14, 27, 36, 17},
                              {-1, 15, 13, 5, 16}
                            };

class Motor
{
  // Class Member Variables
	// These are initialized at startup
  uint8_t motorPin;
  unsigned long OnTime;  // ms
  unsigned long OffTime; // ms

  // These maintain the current state
  byte motorstate;  // motorstate used to set the LED
  unsigned long previousTime;  // will store last time LED was updated

  // Constructor - creates a Motor
  // and initializes the member variables and state
  public:
    Motor(uint8_t pin, unsigned long onT, unsigned long offT)
    {
      motorPin = pin;
      pinMode(motorPin, OUTPUT);

      OnTime = onT;
      OffTime = offT;

      motorstate = LOW;
      previousTime = 0;
    }

    void Update()
    {
      // check to see if it's time to change the state of the LED
      unsigned long currentTime = millis();

      if((motorstate == HIGH) && (currentTime - previousTime >= OnTime)) {
        motorstate = LOW;
        // save the last time you blinked the LED
        previousTime = currentTime;
        digitalWrite(motorPin, motorstate);
      }
      else if((motorstate == LOW) && (currentTime - previousTime >= OffTime)) {
        motorstate = HIGH;
        // save the last time you blinked the LED
        previousTime = currentTime;
        digitalWrite(motorPin, motorstate);
      }
      // Logger
      Serial.printf("Current: %d s, Pin: %d, ON: %d ms, OFF: %d ms\n", currentTime/1000, motorPin, OnTime, OffTime);
    }
};

Motor little1(33, 100, 400);
Motor little2(15, 500, 1500);
Motor little3(32, 350, 1000);
// create a sine func to automatically draw the 3 values of (ON/OFF time interval => frequency) + amplitude


void setup()
{
  Serial.begin(115200);
}

void loop()
{
  little1.Update();
  little2.Update();
  little3.Update();

//  // keep vibrating
//  int patternMode = Serial.read();
//  if (patternMode == '0')
//    turnAllOff();
//  else if (patternMode == '1')
//    led1.Update();
//  else if (patternMode == '2')
//    led2.Update();
}

void turnAllOff()
{
  for (int i=0; i<rows; i++) {
    for (int j=0; j<columns; j++) {
      analogWrite(motors[i][j], 0); // OFF
    }
   }
}

float sineVariable(int amplitude, int degree) {
  return amplitude * sin((PI/180 * degree) + 0);
}

void turnOnPin(int matrix[][columns], int row, int column, unsigned long eventInterval, unsigned char amplitude) {
  if (row < rows && column < columns && amplitude >= 0 && amplitude <= 255) {
      analogWrite(matrix[row][column], amplitude);
  }
  else {
    Serial.println("Input parameters are out of range.");
  }
}
