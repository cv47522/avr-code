/**
 * Example for the ESP32 HTTP(S) Webserver
 *
 * IMPORTANT NOTE:
 * This example is a bit more complex than the other ones, so be careful to 
 * follow all steps.
 * 
 * Make sure to check out the more basic examples like Static-Page to understand
 * the fundamental principles of the API before proceeding with this sketch.
 * 
 * To run this script, you need to
 *  1) Enter your WiFi SSID and PSK below this comment
 *  2) Install the SPIFFS File uploader into your Arduino IDE to be able to
 *     upload static data to the webserver.
 *     Follow the instructions at:
 *     https://github.com/me-no-dev/arduino-esp32fs-plugin
 *  3) Upload the static files from the data/ directory of the example to your
 *     module's SPIFFs by using "ESP32 Sketch Data Upload" from the tools menu.
 *     If you face any problems, read the description of the libraray mentioned
 *     above.
 *     Note: If mounting SPIFFS fails, the script will wait for a serial connection
 *     (open your serial monitor!) and ask if it should format the SPIFFS partition.
 *     You may need this before uploading the data
 *     Note: Make sure to select a partition layout that allows for SPIFFS in the
 *     boards menu
 *  4) Have the ArduinoJSON library installed and available. (Tested with Version 6.17.2)
 *     You'll find it at:
 *     https://arduinojson.org/
 *
 * This script will install an HTTPS Server on your ESP32 with the following
 * functionalities:
 *  - Serve static files from the SPIFFS's data/public directory
 *  - Provide a REST API at /api to receive the asynchronous http requests
 *    - /api/uptime provides access to the current system uptime
 *    - /api/events allows to register or delete events to turn PINs on/off
 *      at certain times.
 *  - Use Arduino JSON for body parsing and generation of responses.
 *  - The certificate is generated on first run and stored to the SPIFFS in
 *    the cert directory (so that the client cannot retrieve the private key)
 */
// (ESP32 Wireless Mode) IP Search APP: Angry IP Scanner
// --- Unity can only send String instead of Int: comment out the validation --- */

// TODO: Configure your WiFi here
//#define WIFI_SSID "TP-LINK_CBDE"
//#define WIFI_PSK  "50268238"
#define WIFI_SSID "aalto open"
#define WIFI_PSK  ""

// Include certificate data
#include "cert.h"
#include "private_key.h"

// We will use wifi
#include <WiFi.h>

// We will use SPIFFS and FS
#include <SPIFFS.h>
#include <FS.h>

// We use JSON as data format. Make sure to have the lib available
#include <ArduinoJson.h>

// Working with c++ strings
#include <string>

// Define the name of the directory for public files in the SPIFFS parition
#define DIR_PUBLIC "/public"
//#define DIR_PUBLIC "/"

// We need to specify some content-type mapping, so the resources get delivered with the
// right content type and are displayed correctly in the browser
char contentTypes[][2][32] = {
  {".html", "text/html"},
  {".css",  "text/css"},
  {".js",   "application/javascript"},
  {".json", "application/json"},
  {".png",  "image/png"},
  {".jpg",  "image/jpg"},
  {"", ""}
};

/* ------------------------------ */
// Includes for the HTTPS/HTTP server
#include <HTTPSServer.hpp>  // HTTPS: only Firefox
#include <HTTPServer.hpp>  // HTTP: Chrome,..all browsers
/* ------------------------------ */

#include <SSLCert.hpp>
#include <HTTPRequest.hpp>
#include <HTTPResponse.hpp>
//#include <util.hpp>
// Required do define ResourceNodes
#include <ResourceNode.hpp>
//#include <StreamUtils.h>

// The HTTPS Server comes in a separate namespace. For easier use, include it here.
using namespace httpsserver;

// Create an SSL certificate object from the files included above
SSLCert cert = SSLCert(
  example_crt_DER, example_crt_DER_len,
  example_key_DER, example_key_DER_len
);

// Create an SSL-enabled server that uses the certificate
// The contstructor takes some more parameters, but we go for default values here.
//HTTPSServer secureServer = HTTPSServer(&cert);

/* ------------------------------ */
// We just create a reference to the server here. We cannot call the constructor unless
// we have initialized the SPIFFS and read or created the certificate
//HTTPSServer * secureServer; // HTTPS
HTTPServer * secureServer;  // HTTP
/* ------------------------------ */

// Declare some handler functions for the various URLs on the server
// The signature is always the same for those functions. They get two parameters,
// which are pointers to the request data (read request body, headers, ...) and
// to the response data (write response, set status code, ...)
void handleSPIFFS(HTTPRequest * req, HTTPResponse * res);
void handleGetUptime(HTTPRequest * req, HTTPResponse * res);
void handleGetEvents(HTTPRequest * req, HTTPResponse * res);
void handlePostEvent(HTTPRequest * req, HTTPResponse * res);
void handleDeleteEvent(HTTPRequest * req, HTTPResponse * res);

// We use the following struct to store GPIO events:
#define MAX_EVENTS 50
struct {
  // is this event used (events that have been run will be set to false)
  bool active;
  // when should it be run?
  unsigned long time;
  // which GPIO should be changed?
  int gpio;
  // and to which state? analog or digital?
  int state;
} events[MAX_EVENTS];

// ************************** Vibration Motor **************************
#include <analogWrite.h>
const int rows = 4;
const int columns = 5;
int ms = 200;

/// v3: Left-Hand, M15: IO4???, M8 (PCB): IO13???
int motorsArr[rows][columns] = {
                           {19, 26, 27, 14, 23}, // thumb~little
                           {5, 13, 33, 12, 22},
                           {21, 17, 16, 18, 4}, // dw1, dw2, up1, dw3, up2
                           {15, 25, 32, -1, -1} // back1~3
                         };

// v3: Right-Hand, back1~3???
// int motorsArr[rows][columns] = {
//                               {23, 12, 33, 13, 19}, // thumb~little
//                               {22, 14, 27, 26, 5},
//                               {21, 18, 4, 32, 17},  // ??dwPalm1, dw2, up1, up2, dw3
//                               {16, 15, 25, -1, -1} // back1~3
//                             };
                          

//const int motorAmplitude = 100; // 0-255

void setup() {
  // For logging
  Serial.begin(115200);

  // Set the pins that we will use as output pins
  for (int i=0; i<rows; i++) {
    for (int j=0; j<columns; j++) {
      pinMode(motorsArr[ i][j], OUTPUT);
    }
  }

  // Try to mount SPIFFS without formatting on failure
  if (!SPIFFS.begin(false)) {
    // If SPIFFS does not work, we wait for serial connection...
    while(!Serial);
    delay(1000);

    // Ask to format SPIFFS using serial interface
    Serial.print("Mounting SPIFFS failed. Try formatting? (y/n): ");
    while(!Serial.available());
    Serial.println();

    // If the user did not accept to try formatting SPIFFS or formatting failed:
    if (Serial.read() != 'y' || !SPIFFS.begin(true)) {
      Serial.println("SPIFFS not available. Stop.");
      while(true);
    }
    Serial.println("SPIFFS has been formated.");
  }
  Serial.println("SPIFFS has been mounted.");

  // Initialize event structure:
  for(int i = 0; i < MAX_EVENTS; i++) {
    events[i].active = false;
    events[i].gpio = 0;
    // events[i].state = LOW; // digital
    events[i].state = 0; // analog
    events[i].time = 0;
  }

  // Connect to WiFi
  Serial.println("Setting up WiFi");
  WiFi.begin(WIFI_SSID, WIFI_PSK);
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.print("Connected. IP=");
  Serial.println(WiFi.localIP());

/* ------------------------------ */
  // Create the server with the certificate we loaded before
  // Use the & operator to store the memory address of the variable called cert, 
  // and assign it to the pointer.
//  secureServer = new HTTPSServer(&cert); // HTTPS
  secureServer = new HTTPServer(); // HTTP
/* ------------------------------ */

  // We register the SPIFFS handler as the default node, so every request that does
  // not hit any other node will be redirected to the file system.
  ResourceNode * spiffsNode = new ResourceNode("", "", &handleSPIFFS);
  secureServer->setDefaultNode(spiffsNode);

  // Add a handler that serves the current system uptime at GET /api/uptime
  ResourceNode * uptimeNode = new ResourceNode("/api/uptime", "GET", &handleGetUptime);
  secureServer->registerNode(uptimeNode);

  // Add the handler nodes that deal with modifying the events:
  ResourceNode * getEventsNode = new ResourceNode("/api/events", "GET", &handleGetEvents);
  secureServer->registerNode(getEventsNode);
  ResourceNode * postEventNode = new ResourceNode("/api/events", "POST", &handlePostEvent);
  secureServer->registerNode(postEventNode);
  ResourceNode * deleteEventNode = new ResourceNode("/api/events/*", "DELETE", &handleDeleteEvent);
  secureServer->registerNode(deleteEventNode);

  Serial.println("Starting server...");
  secureServer->start();
  if (secureServer->isRunning()) {
    Serial.println("Server ready.");
  }
}

void loop() {
  // This call will let the server do its work
  secureServer->loop();

  // Here we handle the events
  unsigned long now = millis() / 1000;
  for (int i = 0; i < MAX_EVENTS; i++) {
    // Only handle active events:
    if (events[i].active) {
      // Only if the counter has recently been exceeded
      if (events[i].time < now) {
//        if (events[i].gpio == vmotor) {
//          analogWrite(vmotor, (events[i].state == 1) ? motorAmplitude : 0);
//        } else {
//          // Apply the state change
//          digitalWrite(events[i].gpio, events[i].state);
//        }

        // digitalWrite(events[i].gpio, events[i].state);
        analogWrite(events[i].gpio, events[i].state);
  
        // Deactivate the event so it doesn't fire again
        events[i].active = false;
      }
    }
  }

  // Other code would go here...
  delay(1);
}
/**
 * This handler function will try to load the requested resource from SPIFFS's /public folder.
 * 
 * If the method is not GET, it will throw 405, if the file is not found, it will throw 404.
 */
void handleSPIFFS(HTTPRequest * req, HTTPResponse * res) {
	
  // We only handle GET here
  if (req->getMethod() == "GET") {
    // Redirect / to /index.html
    std::string reqFile = req->getRequestString()=="/" ? "/index.html" : req->getRequestString();

    // Try to open the file
    std::string filename = std::string(DIR_PUBLIC) + reqFile;

    // Check if the file exists
    if (!SPIFFS.exists(filename.c_str())) {
      // Send "404 Not Found" as response, as the file doesn't seem to exist
      res->setStatusCode(404);
      res->setStatusText("Not found");
      res->println("404 Not Found");
      return;
    }

    File file = SPIFFS.open(filename.c_str());

    // Set length
    res->setHeader("Content-Length", httpsserver::intToString(file.size()));

    // Content-Type is guessed using the definition of the contentTypes-table defined above
    int cTypeIdx = 0;
    do {
      if(reqFile.rfind(contentTypes[cTypeIdx][0])!=std::string::npos) {
        res->setHeader("Content-Type", contentTypes[cTypeIdx][1]);
        break;
      }
      cTypeIdx+=1;
    } while(strlen(contentTypes[cTypeIdx][0])>0);

    // Read the file and write it to the response
    uint8_t buffer[256];
    size_t length = 0;
    do {
      length = file.read(buffer, 256);
      res->write(buffer, length);
    } while (length > 0);

    file.close();
  } else {
    // If there's any body, discard it
    req->discardRequestBody();
    // Send "405 Method not allowed" as response
    res->setStatusCode(405);
    res->setStatusText("Method not allowed");
    res->println("405 Method not allowed");
  }
}

/**
 * This function will return the uptime in seconds as JSON object:
 * {"uptime": 42}
 */
void handleGetUptime(HTTPRequest * req, HTTPResponse * res) {
  // Create a buffer of the size of a JSON object with n members
  // (pretty simple, we have just one key here)
  StaticJsonDocument<JSON_OBJECT_SIZE(1)> doc;
  
  // Set the uptime key to the uptime in seconds
  doc["uptime"] = millis()/1000;
  // Set the content type of the response
  res->setHeader("Content-Type", "application/json");
  // Serialize the object and send the result to Serial.
  // Remember to use *, as we only have a pointer to the HTTPResponse here:
  serializeJson(doc, *res); // output: {"uptime": 42}
}

/**
 * This handler will return a JSON array of currently active events for GET /api/events
 * [{"gpio":27,"state":1,"time":42,"id":0}, ...]
 */
void handleGetEvents(HTTPRequest * req, HTTPResponse * res) {
  // We need to calculate the capacity of the json buffer
  int activeEvents = 0;
  for(int i = 0; i < MAX_EVENTS; i++) {
    if (events[i].active) activeEvents++;

    Serial.print("GPIO: ");
    Serial.print(events[i].gpio);
    Serial.print(", State: ");
    Serial.print(events[i].state);
    Serial.print(", Active: ");
    Serial.print(events[i].active);
    Serial.print(", Time: ");
    Serial.println(events[i].time);
  }

  // For each active event, we need 1 array element with 4 objects
  const size_t capacity = JSON_ARRAY_SIZE(activeEvents) + activeEvents * JSON_OBJECT_SIZE(4);
//  const size_t capacity = 1024;

  // DynamicJsonDocument is created on the heap instead of the stack
  DynamicJsonDocument doc(capacity);
  JsonArray arr = doc.to<JsonArray>();
  for(int i = 0; i < MAX_EVENTS; i++) {
    if (events[i].active) {
      JsonObject eventObj = arr.createNestedObject();
      eventObj["gpio"] = events[i].gpio;
      eventObj["state"] = events[i].state;
      eventObj["time"] = events[i].time;
      // Add the index to allow delete and post to identify the element
      eventObj["id"] = i;
    }
  }

  // Print to response
  res->setHeader("Content-Type", "application/json");
  serializeJson(arr, *res); // output: [{"gpio":27,"state":1,"time":42,"id":0}, ...]
}

void handlePostEvent(HTTPRequest * req, HTTPResponse * res) {
  // We expect an object with 4 elements and add some buffer
  const size_t capacity = JSON_OBJECT_SIZE(4) + 180;
  DynamicJsonDocument reqDoc(capacity);

  // Create buffer to read request
  char * buffer = new char[capacity + 1];
  memset(buffer, 0, capacity+1);

  // Try to read request into buffer
  size_t idx = 0;
  // while "not everything read" or "buffer is full"
  while (!req->requestComplete() && idx < capacity) {
    idx += req->readChars(buffer + idx, capacity-idx);
  }

  // If the request is still not read completely, we cannot process it.
  if (!req->requestComplete()) {
    res->setStatusCode(413);
    res->setStatusText("Request entity too large");
    res->println("413 Request entity too large");
    // Clean up
    delete[] buffer;
    return;
  }

  // Parse the object
  // Extract JSON to an object
  DeserializationError error = deserializeJson(reqDoc, buffer);
  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.f_str());
    return;
  }

  // Check input data types
  bool dataValid = true;
  if (!reqDoc["time"].is<long>() || !reqDoc["gpio"].is<int>() || !reqDoc["state"].is<int>()) {
    dataValid = false;
  }
	
  // Check actual values
  unsigned long eTime = 0;
  int eGpio = 0;
  // int eState = LOW; // digital
  int eState = 0; // analog
  if (dataValid) {
    eTime = reqDoc["time"];
//    if (eTime < millis()/1000) dataValid = false;

    eGpio = reqDoc["gpio"];
    // if (!(eGpio == GPIO_1 || eGpio == GPIO_2 || eGpio == GPIO_3 || eGpio == GPIO_4 || eGpio == GPIO_5)) dataValid = false;
//    for (int i = 0; i < rows; i++) {
//      for (int j = 0; j < columns; j++) {
//        if (eGpio != motorsArr[ i][j]) dataValid = false;
//      }
//    }

    eState = reqDoc["state"];
    // if (eState != HIGH && eState != LOW) dataValid = false;
    if (eState < 0 || eState > 255) dataValid = false;
  }

  Serial.println(reqDoc["state"].as<int>());
  Serial.print("eState: ");
  Serial.println(eState);
  Serial.println("before 404");
    
  // Clean up, we don't need the buffer any longer
  delete[] buffer;

  // If something failed: 400
  /* ------------------------------------------- */
  dataValid = true;
  /* ------------------------------------------- */
  // ??? command line POST not working but browser UI works!
  if (!dataValid) {
    res->setStatusCode(400);
    res->setStatusText("Bad Request");
    res->println("400 Bad Request");

    res->print("dataValid: ");
    res->println(dataValid);
    res->print("eTime: ");
//    res->println(eTime); // 0
    res->println(reqDoc["time"].as<long>());
    res->print("eGpio: "); // 0
//    res->println(eGpio);
    res->println(reqDoc["gpio"].as<int>());
    res->print("eState: ");
//    res->println(eState); // 0
    res->println(reqDoc["state"].as<int>());
    return;
  }

   Serial.println(reqDoc["state"].as<int>());
   Serial.print("eState: ");
   Serial.println(eState);

  // Try to find an inactive event in the list to write the data to
  int eventID = -1;
  for(int i = 0; i < MAX_EVENTS && eventID == -1; i++) {
    if (!events[i].active) {
      eventID = i;
//      events[i].gpio = eGpio;  // 0, not working w/ httpPie POST command
      events[i].gpio = reqDoc["gpio"].as<int>(); // works w/ httpPie POST command
//      events[i].time = eTime;  // 0
      events[i].time = reqDoc["time"].as<long>();
//      events[i].state = eState;  // 0
      events[i].state = reqDoc["state"].as<int>();
      events[i].active = true;
    }
  }

  // Check if we could store the event
  if (eventID > -1) {
    // Create a buffer for the response
    StaticJsonDocument<JSON_OBJECT_SIZE(4)> resDoc;

    // Set the uptime key to the uptime in seconds
    // ??? always returns 0
//    resDoc["gpio"] = events[eventID].gpio;
//    resDoc["state"] = events[eventID].state;
//    resDoc["time"] = events[eventID].time;
//    resDoc["id"] = eventID;

    resDoc["gpio"] = reqDoc["gpio"].as<int>();
    resDoc["state"] = reqDoc["state"].as<int>();
    resDoc["time"] = reqDoc["time"].as<long>();
    resDoc["id"] = eventID;

    // Write the response
    res->setHeader("Content-Type", "application/json");
    serializeJson(resDoc, *res); // output: {"gpio": 2, "state": "HIGH", "time": 42, "id": 1}

  } else {
    // We could not store the event, no free slot.
    res->setStatusCode(507);
    res->setStatusText("Insufficient storage");
    res->println("507 Insufficient storage");

  }
}

/**
 * This handler will delete an event (meaning: deactive the event)
 */
void handleDeleteEvent(HTTPRequest * req, HTTPResponse * res) {
  // Access the parameter from the URL. See Parameters example for more details on this
  ResourceParameters * params = req->getParams();
  size_t eid = std::atoi(params->getPathParameter(0).c_str());

  if (eid < MAX_EVENTS) {
    // Set the inactive flag
    events[eid].active = false;
    // And return a successful response without body
    res->setStatusCode(204);
    res->setStatusText("No Content");
  } else {
    // Send error message
    res->setStatusCode(400);
    res->setStatusText("Bad Request");
    res->println("400 Bad Request");
  }
}

// ************************** Vibration Motor **************************
void turnAllMotorsOff() {
  for (int i=0; i<rows; i++) {
    for (int j=0; j<columns; j++) {
      analogWrite(motorsArr[ i][j], 0); // OFF
    }
   }
}
