// ESP32 Touch Test
// Just test touch pin - Touch0 is T0 which is on GPIO 4.

#define THUMB 27 // 4
#define POINTER 32 // 12
#define MIDDLE_FINGER 15 // 13
#define RING_FINGER 14 // 14
#define PINKY 33 // 15

void setup() {
  Serial.begin(115200);
  delay(1000); // give me time to bring up serial monitor
  Serial.println("ESP32 Touch Test");

  pinMode(2, OUTPUT);
}

void loop() {
  Serial.print("Thumb: ");
  Serial.println(touchRead(THUMB));  // get value of Touch 0 pin = GPIO 4
  Serial.print("Pointer: ");
  Serial.println(touchRead(POINTER));
  Serial.print("Middle Finger: ");
  Serial.println(touchRead(MIDDLE_FINGER));
  Serial.print("Ring Finger: ");
  Serial.println(touchRead(RING_FINGER));
  Serial.print("Pinky: ");
  Serial.println(touchRead(PINKY));

  if (touchRead(THUMB)<30 || touchRead(POINTER)<30 || 
      touchRead(MIDDLE_FINGER)<30 || touchRead(RING_FINGER)<30 || 
      touchRead(PINKY)<30) 
  {
    digitalWrite(2, HIGH);
  }else {
    digitalWrite(2, LOW);
  }
  
  delay(200);
}
