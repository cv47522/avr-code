// !!!Use Firefox browser!!!

#include <analogWrite.h>
const int rows = 4;
const int columns = 5;
unsigned long currentTime;
unsigned long previousTime = 0;

// 2D array (row, column): store relative location => make a visual interface
// or 3D array (row, column, layer: front/back)

// for KiCad ESP32 Hostboard
/// v3: Left-Hand, M15: IO4???, M8 (PCB): IO13???
int motorsArr[rows][columns] = {
                           {19, 26, 27, 14, 23}, // thumb~little
                           {5, 13, 33, 12, 22},
                           {21, 17, 16, 18, 4}, // dw1, dw2, up1, dw3, up2
                           {15, 25, 32, -1, -1} // back1~3
                         };

// v3: Right-Hand, back1~3???
// int motorsArr[rows][columns] = {
//                               {23, 12, 33, 13, 19}, // thumb~little
//                               {22, 14, 27, 26, 5},
//                               {21, 18, 4, 32, 17},  // ??dwPalm1, dw2, up1, up2, dw3
//                               {16, 15, 25, -1, -1} // back1~3
//                             };
                          

// describe absolute location of the finger joints: define a configuration file
// which descibes the difference between different setups
// how to describe quantitative data

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  // this for loop works correctly with an array of any type or size
  for (int i=0; i<rows; i++) {
    for (int j=0; j<columns; j++) {
      pinMode(motorsArr[i][j], OUTPUT);
    }
  }
  pinMode(13, OUTPUT);// important!!!
  turnAllOff();// important!!!
}

void loop() {
  /* Updates frequently */
  currentTime = millis();

  turnAllOff();

  int patternMode = Serial.read();
  switch (patternMode) {
    case '1': // Pattern 1: Heart Beat
    for (int times=0; times<20; times++) {
      for (int i=0; i<rows; i++) {
        for (int j=0; j<columns; j++) {
          analogWrite(motorsArr[i][j], 150); // ON
        }
      }
      delay(200);
      turnAllOff();
      delay(200);
      for (int i=0; i<rows; i++) {
        for (int j=0; j<columns; j++) {
          analogWrite(motorsArr[i][j], 100); // ON
        }
      }
      delay(200);
      turnAllOff();
      delay(200);
    }
    break;

    case '2': // Pattern 2: Iterate through each element of rows (row 1 -> row 3)
      for (int i=0; i<rows; i++) {
        for (int j=0; j<columns; j++) {
          analogWrite(motorsArr[i][j], 150); // amplitude
          Serial.print("Mode 1 turn ON pin: ");
          Serial.println(motorsArr[i][j]);
          delay(200);
          analogWrite(motorsArr[i][j], 0); // OFF
          Serial.print("OFF: ");
          Serial.println(motorsArr[i][j]);
          delay(200);
        }
       }
       turnAllOff();
       break;

     case '3': // Pattern 3: Two users touch each other
        analogWrite(motorsArr[0][2], 180); // middle finger: top joint
        delay(600);
        analogWrite(motorsArr[0][2], 0);
        analogWrite(motorsArr[0][1], 120); // pointer & ring finger: top joint
        analogWrite(motorsArr[07][3], 120);
        delay(1500);
        analogWrite(motorsArr[0][1], 0); // pointer & ring finger: top joint
        analogWrite(motorsArr[0][3], 0);
        analogWrite(motorsArr[0][0], 80); // thumb & littele finger: top joint
        analogWrite(motorsArr[0][4], 80);
        delay(3000);
        turnAllOff();
        break;

      case '4': // Pattern 4: Spread
        analogWrite(motorsArr[0][2], 180); // middle finger: top joint
        delay(600);
        analogWrite(motorsArr[0][2], 0);
        analogWrite(motorsArr[0][1], 150); // pointer & ring finger: top joint
        analogWrite(motorsArr[0][3], 150);
        analogWrite(motorsArr[1][2], 150);
        delay(1500);
        analogWrite(motorsArr[0][1], 0); // pointer & ring finger: top joint
        analogWrite(motorsArr[0][3], 0);
        analogWrite(motorsArr[1][2], 0);
        analogWrite(motorsArr[1][1], 100); // pointer & ring finger: middle joint
        analogWrite(motorsArr[1][3], 100);
        analogWrite(motorsArr[0][4], 80);
        analogWrite(motorsArr[2][2], 100);
        delay(2000);
        analogWrite(motorsArr[1][1], 0); // pointer & ring finger: middle joint
        analogWrite(motorsArr[1][3], 0);
        analogWrite(motorsArr[0][4], 0);
        analogWrite(motorsArr[2][2], 0);
        analogWrite(motorsArr[2][1], 70);
        analogWrite(motorsArr[2][3], 70);
        analogWrite(motorsArr[1][4], 60);
        analogWrite(motorsArr[2][4], 50);
        analogWrite(motorsArr[0][0], 50);
        analogWrite(motorsArr[1][0], 30);
        delay(3000);

      case '5': // Pattern 3: Two users touch each other
//        turnOnPin(motorsArr, 0, 2, 100, 200); // middle finger: top joint
//        turnOnPin(motorsArr, 0, 1, 300, 200); // pointer & ring finger: top joint
//        turnOnPin(motorsArr, 0, 3, 300, 200);
//        turnOnPin(motorsArr, 0, 0, 800, 100); // thumb & littele finger: top joint
//        turnOnPin(motorsArr, 0, 4, 800, 100);
        currentTime = 0;
        turnOnPin(motorsArr, 0, 4, 300, 200);
        turnOnPin(motorsArr, 2, 4, 1000, 150);
        delay(5000);
//        turnAllOff();
        break;

      case '6': // top fingertips: heartbeat
        for (int times=0; times<20; times++) {
          for (int i=0; i<1; i++) {
            for (int j=0; j<columns; j++) {
              analogWrite(motorsArr[i][j], 150); // ON
            }
          }
          delay(200);
          turnAllOff();
          delay(200);
          for (int i=0; i<1; i++) {
            for (int j=0; j<columns; j++) {
              analogWrite(motorsArr[i][j], 100); // ON
            }
          }
          delay(200);
          turnAllOff();
          delay(200);
        }
        break;

      case '7': // middle knuckles: heartbeat
        for (int times=0; times<20; times++) {
          for (int i=1; i<2; i++) {
            for (int j=0; j<columns; j++) {
              analogWrite(motorsArr[i][j], 150); // ON
            }
          }
          delay(200);
          turnAllOff();
          delay(200);
          for (int i=1; i<2; i++) {
            for (int j=0; j<columns; j++) {
              analogWrite(motorsArr[i][j], 100); // ON
            }
          }
          delay(200);
          turnAllOff();
          delay(200);
        }
        break;

      case '8': // ???all palm points
        for (int times=0; times<20; times++) {
//          for (int i=2; i<3; i++) {
            for (int j=0; j<columns; j++) {
              analogWrite(motorsArr[2][j], 150); // ON
            }
//          }
          delay(200);
          turnAllOff();
          delay(200);
          for (int i=2; i<3; i++) {
            for (int j=0; j<columns; j++) {
              analogWrite(motorsArr[i][j], 100); // ON
            }
          }
          delay(200);
          turnAllOff();
          delay(200);
        }
        break;

      case '9': // all back points
        for (int times=0; times<20; times++) {
          for (int i=3; i<4; i++) {
            for (int j=0; j<columns; j++) {
              analogWrite(motorsArr[i][j], 150); // ON
            }
          }
          delay(200);
          turnAllOff();
          delay(200);
          for (int i=3; i<4; i++) {
            for (int j=0; j<columns; j++) {
              analogWrite(motorsArr[i][j], 100); // ON
            }
          }
          delay(200);
          turnAllOff();
          delay(200);
        }
        break;

      case 'a': // ???all back points: iteration
        for (int times=0; times<10; times++) {
          for (int j=0; j<columns; j++) {
            analogWrite(motorsArr[3][j], 150); // ON
            delay(200);
  //          turnAllOff();
             analogWrite(motorsArr[3][j], 0); // ON
  //          delay(200);
          }
        }
        break;

       case 'b': // all palm points: iteration
        for (int times=0; times<10; times++) {
          for (int j=0; j<columns; j++) {
            analogWrite(motorsArr[2][j], 150); // ON
            delay(200);
  //          turnAllOff();
             analogWrite(motorsArr[2][j], 0); // ON
  //          delay(200);
          }
        }
        break;



      default: // turn all OFF
        for (int i=0; i<rows; i++) {
          for (int j=0; j < columns; j++) {
            analogWrite(motorsArr[i][j], 0); // OFF
          }
         }
        Serial.println("OFF Mode");
  }
}

void turnAllOff() {
  for (int i=0; i<rows; i++) {
    for (int j=0; j<columns; j++) {
      analogWrite(motorsArr[i][j], 0); // OFF
    }
   }
}

void turnOnPin(int matrix[][columns], int row, int column, unsigned long eventInterval, unsigned char amplitude) {
  if (row < rows && column < columns && amplitude >= 0 && amplitude <=255) {
//     if (currentTime - previousTime >= eventInterval) {
  currentTime = millis();
  if (currentTime >= eventInterval) {
      /* Event code */
      analogWrite(matrix[row][column], amplitude);

      /* Update the timing for the next time around */
//      previousTime = currentTime;
    }
    Serial.printf("%ds, %d, %d, %d, %d\n", currentTime/1000, row, column, eventInterval, amplitude); // Logger
  }
  else {
    Serial.println("Input parameters are out of range.");
  }
}
