/*
 delay(): blocking
 millis(): non-blocking, other functions can act at the same time.
 */

#define LED 16

// Variables will change
int ledState = LOW;

// Generally, you should use "unsigned long" for variables that hold time
// The value will quickly become too large for an int to store
unsigned long previousMillis = 0;

// interval at which to blink (milliseconds)
const long interval = 1000;

void setup() {
  pinMode(LED, OUTPUT);
}

void loop() {
  unsigned long currentMillis = millis();

  // check to see if it's time to blink the LED; that is, if the
  // difference between the current time and last time you blinked
  // the LED is bigger than the interval at which you want to
  // blink the LED.
  if (currentMillis - previousMillis >= interval) {
    // save the last time you blinked the LED
    previousMillis = currentMillis;
    
    ledState = (ledState == LOW) ? HIGH : LOW;
  }

  digitalWrite(LED, ledState);
}
