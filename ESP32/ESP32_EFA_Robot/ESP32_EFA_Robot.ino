#define MR_EN 5  // control speed
#define MR_C1 6  // contril direction
#define MR_C2 7

#define ML_EN 9  // control speed
#define ML_C1 10 // contril direction
#define ML_C2 11

#define TRIG_PIN 3
#define ECHO_PIN 2

long duration;
int distance;

int light;
int lightThreshold = 700;

void setup() {
  Serial.begin(9600);
  
  pinMode(MR_EN, OUTPUT);
  pinMode(MR_C1, OUTPUT);
  pinMode(MR_C2, OUTPUT);

  pinMode(ML_EN, OUTPUT);
  pinMode(ML_C1, OUTPUT);
  pinMode(ML_C2, OUTPUT);

  pinMode(TRIG_PIN, OUTPUT);
  pinMode(ECHO_PIN, INPUT);

  leftSpeed(255);
  rightSpeed(255);
}

void loop() {
  // read the sensors
  readUltrasonic(true,400);
  readLight();

  // only move if the light level is high enough
  if(light > lightThreshold){
    // turn left if there is something in the way
    if(distance > 30){
        goForward();
      }else{
        goLeft();  
      }
  }else{
    stopAll();
  }
  
}

void goForward(){
  leftMotorForward();
  rightMotorForward();
}

void goBackward(){
  leftMotorBackward();
  rightMotorBackward();
}

void goLeft(){
  rightMotorForward();
  leftMotorBackward();
}

void goRight(){
  leftMotorForward();
  rightMotorBackward();
}

void stopAll(){
  leftMotorStop();
  rightMotorStop();
}

void leftMotorForward(){
  digitalWrite(ML_C1, HIGH);
  digitalWrite(ML_C2, LOW);
}

void leftMotorBackward(){
  digitalWrite(ML_C1, LOW);
  digitalWrite(ML_C2, HIGH);
}

void leftMotorStop(){
  digitalWrite(ML_C1, LOW);
  digitalWrite(ML_C2, LOW);
}

void rightMotorStop(){
  digitalWrite(MR_C1, LOW);
  digitalWrite(MR_C2, LOW);
}

void rightMotorForward(){
  digitalWrite(MR_C1, HIGH);
  digitalWrite(MR_C2, LOW);
}


void rightMotorBackward(){
  digitalWrite(MR_C1, LOW);
  digitalWrite(MR_C2, HIGH);
}

void leftSpeed(int mSpeed){
 analogWrite(ML_EN, mSpeed);
}

void rightSpeed(int mSpeed){
 analogWrite(MR_EN, mSpeed);
}

void readUltrasonic(bool shouldPrint, int maxDistance) {
  // Clear the TRIG_PIN
  digitalWrite(TRIG_PIN, LOW);
  delayMicroseconds(2);

  // Set the TRIG_PIN HIGH for 10 microseconds
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);

  // Read the ECHO_PIN, returns the sound wave travel time in microseconds
  duration = pulseIn(ECHO_PIN, HIGH);

  // Calculate the distance
  // The calculation we use below is an approximation of the real formula:
  // duration * 0.034 / 2.0
  // (0.017 equals roughly to 1/58)
  // We do it this way to avoid having to use floating point numbers.
  int tempDistance = duration / 58;

  // sometimes the sensor gives strange values. This just ignores all values larger than the maxDistance.
  if (tempDistance < maxDistance) {
    distance = tempDistance;
  }

  if (shouldPrint) {
    // Prints the distance on the Serial Monitor (cm)
    Serial.print("Distance: ");
    Serial.print(distance);
    Serial.println("cm");
  }
}

void readLight(){
  light = analogRead(A0);
  Serial.print("light: ");
  Serial.println(light);
}
