#include <analogWrite.h>
const int rows = 3;
const int columns = 5;

// 2D array (row, column): store relative location => make a visual interface
// or 3D array (row, column, layer: front/back)
int LEDs[rows][columns] = {
                            {25, 32, 33, 34, 21}, 
                            {26, 14, 27, 36, 17}, 
                            {-1, 15, 13, 5, 16}
                          };                                                                 

// describe absolute location of the finger joints: define a configuration file
// which descibes the difference between different setups
// how to describe quantitative data

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  // this for loop works correctly with an array of any type or size
  for (int i=0; i<rows; i++) {
    for (int j=0; j<columns; j++) {
      pinMode(LEDs[i][j], OUTPUT);
    }
  }
}

void loop() {
  // put your main code here, to run repeatedly:
  turnAllOff();

  int patternMode = Serial.read();
  switch (patternMode) {
    case '1': // Pattern 1: Heart Beat (should add more 10% intensity and 5% variance OFF time between each beat)
    for (int times=0; times<50; times++) {
      for (int i=0; i<rows; i++) {
        for (int j=0; j<columns; j++) {
          analogWrite(LEDs[i][j], 250); // ON
        }
      }
      delay(200);
      turnAllOff();
      delay(50);
      for (int i=0; i<rows; i++) {
        for (int j=0; j<columns; j++) {
          analogWrite(LEDs[i][j], 100); // ON
        }
      }
      delay(300);
      turnAllOff();
      delay(500);
    }
    break;
    
    case '2': // Pattern 2: Iterate through each element of rows (row 1 -> row 3)
      for (int i=0; i<rows; i++) {
        for (int j=0; j<columns; j++) {
          analogWrite(LEDs[i][j], 200); // amplitude
          Serial.print("Mode 1 turn ON pin: ");
          Serial.println(LEDs[i][j]);
          delay(200);
          analogWrite(LEDs[i][j], 0); // OFF
          Serial.print("OFF: ");
          Serial.println(LEDs[i][j]);
          delay(200);
        }
       }
       turnAllOff();
       break;
        
     case '3': // Pattern 3: Two users touch each other
        analogWrite(LEDs[0][2], 250); // middle finger: top joint
        delay(200);
        analogWrite(LEDs[0][1], 200); // pointer & ring finger: top joint
        analogWrite(LEDs[0][3], 200);
        delay(500);
        analogWrite(LEDs[0][0], 100); // thumb & littele finger: top joint
        analogWrite(LEDs[0][4], 100);
        delay(5000);
        turnAllOff();
        break;

      case '4': // Pattern 4: Spread
        analogWrite(LEDs[0][2], 250); // middle finger: top joint
        delay(200);
        analogWrite(LEDs[0][1], 200); // pointer & ring finger: top joint
        analogWrite(LEDs[0][3], 200);
        analogWrite(LEDs[1][2], 200);
        delay(500);
        analogWrite(LEDs[1][1], 100); // pointer & ring finger: middle joint
        analogWrite(LEDs[1][3], 100);
        analogWrite(LEDs[0][4], 80);
        analogWrite(LEDs[2][2], 100);
        delay(1000);
        analogWrite(LEDs[2][1], 70);
        analogWrite(LEDs[2][3], 70);
        analogWrite(LEDs[1][4], 60);
        analogWrite(LEDs[2][4], 50);
        analogWrite(LEDs[0][0], 50);
        analogWrite(LEDs[1][0], 30);
        delay(5000);

       case '5': // Pattern 4: Spread
        analogWrite(LEDs[0][0], 250);
        delay(5000);
        break;
        
      default: // turn all OFF
        for (int i=0; i<rows; i++) {
          for (int j=0; j<columns; j++) {
            analogWrite(LEDs[i][j], 0); // OFF
          }
         }
        Serial.println("OFF Mode");
  }
}

void turnAllOff() {
  for (int i=0; i<rows; i++) {
    for (int j=0; j<columns; j++) {
      analogWrite(LEDs[i][j], 0); // OFF
    }
   }
}
