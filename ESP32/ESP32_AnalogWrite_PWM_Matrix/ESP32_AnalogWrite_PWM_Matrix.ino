#include <analogWrite.h>
const int LEDs[] = {15, 32, 14}; // 2D array (row, column): store relative location => make a visualinterface                                                                
// or 3D array (row, column, layer:front, back)
// describe absolute location of the finger joints: configuration file (.sh file), descibe the difference of different setups
// how to describe quantitive data
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  // this for loop works correctly with an array of any type or size
  for (byte i = 0; i < (sizeof(LEDs) / sizeof(LEDs[0])); i++) {
    pinMode(LEDs[i], OUTPUT);
  }
}

void loop() {
  // put your main code here, to run repeatedly:
  // this for loop works correctly with an array of any type or size
  for (byte i = 0; i < (sizeof(LEDs) / sizeof(LEDs[0])); i++) {
    analogWrite(LEDs[i], 200); // amplitude
    Serial.println(LEDs[i]);
    delay(200); // 200: heart beat
    analogWrite(LEDs[i], 0); // OFF
    Serial.println(LEDs[i]);
    delay(200);
  }
}
