/*
  Listfiles

  This example shows how print out the files in a
  directory on a SD card

  The circuit:
   SD card attached to SPI bus as follows:
 ** MOSI - pin 11
 ** MISO - pin 12
 ** CLK - pin 13
 ** CS - pin 4 (for MKRZero SD: SDCARD_SS_PIN)

  created   Nov 2010
  by David A. Mellis
  modified 9 Apr 2012
  by Tom Igoe
  modified 2 Feb 2014
  by Scott Fitzgerald

  This example code is in the public domain.

*/
#include <SPI.h>
#include <SD.h>

const int CS = 33; // CS/SS pin: 33 (Adafruit ESP32 Feather), 4 (Arduino UNO)
const int LED1 = 13; // red
const int LED2 = 7; //21; // green
const int VR = A0; // potentiometer

File logFile;

void setup() {
  // connect at 115200 so we can read the GPS fast enough and
  // echo without dropping chars also spit it out
  Serial.begin(115200);
  Serial.println("\r\nAnalog logger test");
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  
  // see if the card is present and can be initialized:
  Serial.print("Initializing SD card...");
  if (!SD.begin(CS)) {
    Serial.println("initialization failed!");
    error(2);
  }
  Serial.println("initialization done.");

  char filename[15];
  strcpy(filename, "/ANOLOG00.TXT");
  for (uint8_t i=0; i<100; i++) {
    filename[7] = '0' + i/10;
    filename[8] = '0' + i%10;
    // create if doesn't exist, don't open existing, write, sync after write
    if (!SD.exists(filename)) {
      break;
    }
  }
  logFile = SD.open(filename, FILE_WRITE);
  if (!logFile) {
    Serial.print("Couldn't create "); Serial.println(filename);
    error(3);
  }
  Serial.print("Writing to "); Serial.println(filename);
  Serial.println("Ready!");
}

void loop() {
  digitalWrite(LED2, HIGH);
  logFile.print("A0 = "); logFile.println(analogRead(VR));
  logFile.flush();
  Serial.print("A0 = "); Serial.println(analogRead(VR));
  digitalWrite(LED2, LOW);
  delay(200);
}


// blink out an error code
void error(uint8_t errNo) {
  while(1) {
    uint8_t i;
    for (i=0; i<errNo; i++) {
      digitalWrite(LED1, HIGH);
      delay(100);
      digitalWrite(LED1, LOW);
      delay(100);
    }
    for (i=errNo; i<10; i++) {
      delay(200);
    }
  }
}
