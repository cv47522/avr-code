#include <analogWrite.h>

#define soundSensor 34
#define LED 16

const int rangeMax = 1900; //4095
const int rangeMin = 1600;
int sensorValue = 0;        // value read from the pot
int outputValue = 0;        // value outpu

void setup() {
  Serial.begin(115200);
  pinMode(LED, OUTPUT);
  pinMode(soundSensor, INPUT); // uncomment for digital sound sensor
}

void loop() {
//  sensorValue = digitalRead(soundSensor);
//  digitalWrite(LED, (sensorValue==HIGH) ? HIGH : LOW);
//  digitalWrite(LED, (sensorValue==HIGH) ? LOW : HIGH); //KSM043

  sensorValue = analogRead(soundSensor);
//  sensorValue = (sensorValue > rangeMax) ? rangeMax : sensorValue;
//  outputValue = map(sensorValue, rangeMax, rangeMin, 0, 255);
//  sensorValue = (sensorValue < rangeMin) ? rangeMin : sensorValue;
  outputValue = map(sensorValue, rangeMin, rangeMax, 0, 255);
  analogWrite(LED, outputValue);

  
  Serial.println(sensorValue);

}
