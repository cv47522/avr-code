#define MIC 36

int micVal = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  delay(500);
}

void loop() {
  // put your main code here, to run repeatedly:
  micVal = analogRead(MIC);
  Serial.println(micVal);
  delay(200);
}
