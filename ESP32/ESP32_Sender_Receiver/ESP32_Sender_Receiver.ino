/*
  Rui Santos
  Complete project details at https://RandomNerdTutorials.com/esp-now-esp32-arduino-ide/
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files.
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
*/

#include <esp_now.h>
#include <WiFi.h>
#include <analogWrite.h>

// REPLACE WITH YOUR RECEIVER MAC Address
uint8_t broadcastAddress[] = {0x24, 0x6F, 0x28, 0x7A, 0xA0, 0xF4};
//uint8_t broadcastAddress[] = {0x24, 0x6F, 0x28, 0x7A, 0xB1, 0x78};

#define PIEZO 4 //2 //ADC pin: analog (0-4095)
#define LED 16 // PWM pin

//const int threshold = 35;
const int rangeMax = 35;
const int rangeMin = 6;
int sensorValue = 0;        // value read from the pot
int outputValue = 0;        // value output to the PWM (analog out)

int myData; // Sensor State

// callback when data is sent
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {
  Serial.print("\r\nLast Packet Send Status:\t");
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
}

// callback function that will be executed when data is received
void OnDataRecv(const uint8_t * mac, const uint8_t *incomingData, int len) {
  memcpy(&myData, incomingData, sizeof(myData));
  Serial.print("LED Value: ");
  Serial.println(myData);
  
  // change the analog out value:
  analogWrite(LED, myData);
}

 
void setup() {
  // Init Serial Monitor
  Serial.begin(115200);
 
  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);

  // Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  // Once ESPNow is successfully Init, we will register for Send CB to
  // get the status of Trasnmitted packet
  esp_now_register_send_cb(OnDataSent);
  
  // Register peer
  esp_now_peer_info_t peerInfo;
  memcpy(peerInfo.peer_addr, broadcastAddress, 6);
  peerInfo.channel = 0;  
  peerInfo.encrypt = false;
  
  // Add peer        
  if (esp_now_add_peer(&peerInfo) != ESP_OK){
    Serial.println("Failed to add peer");
    return;
  }

  // Register for a callback function that will be called when data is received
  esp_now_register_recv_cb(OnDataRecv);
}
 
void loop() {
  getSensorValue();
 
  // Send message via ESP-NOW
  esp_err_t result = esp_now_send(broadcastAddress, (uint8_t *) &outputValue, sizeof(outputValue));
   
  if (result == ESP_OK) {
    Serial.print("Sent with success, Sensor Value: ");
  }
  else {
    Serial.println("Error sending the data");
  }

//  Serial.print(outputValue);
  Serial.print(sensorValue);
  delay(200);
}

void getSensorValue() {
  //  sensorValue = analogRead(PIEZO);
  sensorValue = touchRead(PIEZO);
  sensorValue = (sensorValue > rangeMax) ? rangeMax : sensorValue;
  // map it to the range of the analog out:
//  outputValue = map(sensorValue, 0, 4095, 0, 255);
  outputValue = map(sensorValue, rangeMax, rangeMin, 0, 255);
}
