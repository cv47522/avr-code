/*
 * tutorial: https://randomnerdtutorials.com/esp32-pir-motion-sensor-interrupts-timers/
 * delay(): blocking
 * millis(): non-blocking, other functions can act at the same time.
 */
#define intervalSeconds 5

#define LED 16
#define PIEZO 17

// Timer: Auxiliary variables
unsigned long currentMillis = millis();
unsigned long lastTrigger = 0;
bool startTimer = false;

// If the method is not tagged with IRAM_ATTR it will NOT be called during operations involving flash read/write.
void IRAM_ATTR detectsMovement() {
  Serial.println("Motion Detected!!");
  digitalWrite(LED, HIGH);
  startTimer = true;
  lastTrigger = millis();
}


void setup() {
  Serial.begin(115200);

  pinMode(PIR, INPUT_PULLUP); // pull-up R: around 10k ohms

  // PIR: digital output
  attachInterrupt(digitalPinToInterrupt(PIR), detectsMovement, RISING);
  
  pinMode(LED, OUTPUT);
  digitalWrite(LED, LOW);
}


void loop() {
  currentMillis = millis();

  if (startTimer && (currentMillis - lastTrigger > (intervalSeconds*1000))) {
    Serial.println("Motion Stopped...");
    digitalWrite(LED, LOW);
    startTimer = false;
  }
}
