const modelParams = {
  flipHorizontal: false,   // flip e.g for video
  imageScaleFactor: 0.7,  // reduce input image size for gains in speed.
  maxNumBoxes: 1,        // maximum number of boxes to detect
  iouThreshold: 0.5,      // ioU threshold for non-max suppression
  scoreThreshold: 0.79,    // confidence threshold for predictions.
}

navigator.getUserMedia = navigator.getUserMedia ||
                         navigator.webkitGetUserMedia ||
                         navigator.mozGetUserMedia;

//Select everything in my html
const video = document.querySelector('#video');
const canvas = document.querySelector('#canvas');
const context = canvas.getContext('2d');
let model;

handTrack.startVideo(video)
	.then(status => {
		if(status){
			navigator.getUserMedia({video: {}}, stream => {
				video.srcObject = stream;
				setInterval(runDetection, 100);
			},
			err => console.log(err)
			);
		}
	});
/************* */
var gateway = `ws://${window.location.hostname}/ws`;
var websocket;
function initWebSocket() {
  console.log('Trying to open a WebSocket connection...');
  websocket = new WebSocket(gateway);
  websocket.onopen    = onOpen;
  websocket.onclose   = onClose;
  websocket.onmessage = onMessage; // <-- add this line
}
function onOpen(event) {
  console.log('Connection opened');
}

function onClose(event) {
  console.log('Connection closed');
  setTimeout(initWebSocket, 2000);
}
function onMessage(event) {
  var state;
  if (event.data == "1"){
	state = "ON";
  }
  else{
	state = "OFF";
  }
  document.getElementById('state').innerHTML = state;
}

window.addEventListener('load', onLoad);

function onLoad(event) {
  initWebSocket();
  initButton();
}

function initButton() {
  document.getElementById('button').addEventListener('click', toggle);
}

function toggle(){
  websocket.send('toggle');
}

/************ */
	function runDetection(){
		model.detect(video).then(predictions => {
			console.log(predictions);
			model.renderPredictions(predictions, canvas, context, video);
			if(predictions.length > 0){
				let hand1 = predictions[0].bbox;
				let x = hand1[0];
				let y = hand1[1];
				console.log(x);
				console.log(y);
				/*if(y > 0){
					if (x < 200){
						document.getElementsByTagName("h3")[0].style.opacity = "0%";
					} else if(x > 400){
							document.getElementsByTagName("h3")[0].style.opacity = "100%";
						} else if(x > 300){
								document.getElementsByTagName("h3")[0].style.opacity = "60%";
							} else if(x > 200){
									document.getElementsByTagName("h3")[0].style.opacity = "30%";
								}
				}*/
				if(x >= 200 && x <= 300){
					if(y >= 100 && y <= 200){
						document.getElementsByTagName("h3")[0].style.opacity = "100%";
						console.log("in area");
					} else {
						document.getElementsByTagName("h3")[0].style.opacity = "0%";
					} 
				} else {
						document.getElementsByTagName("h3")[0].style.opacity = "0%";
					}
			}
		});
	}

handTrack.load(modelParams).then(lmodel => {
	model = lmodel;
	});
/////////////////////////////
// Local copy of the event list
let events = [];
// Local copy of the uptime
let uptime = 0;

// This function synchronizes the uptime every 10s
function updateUptime() {
  $.ajax({
	// Target
	url: '/api/uptime',
	// Method
	type: 'GET',
	// Data we expect back
	dataType: 'json',
  }).done(res => {
	// Update now and keep track for 10s
	$('#uptime').text(res.uptime);
	for(let i = 0; i < 10; i++) {
	  const t = res.uptime + i;
	  setTimeout(() => {
		uptime = t;
		$('#uptime').text(t);
		updateTable();
	  }, i*1000);
	}
  }).always(() => {
	// Synchronize again after 10s
	setTimeout(updateUptime, 10000);
  });
}

// This function will update the event list every 60s
function updateEventlist() {
  $.ajax({
	// Target
	url: '/api/events',
	// Method
	type: 'GET',
	// Data we expect back
	dataType: 'json',
  }).done(res => {
	// Replace list in-place
	while(events.length>0) events.pop();
	res.forEach(e => events.push(e));
	updateTable();
  }).always(() => {
	// Poll again after 60s
	// The add and delete method keep the list synchronized.
	setTimeout(updateUptime, 60000);
  });
}

// Adds an event based on the form
function addEvent() {
  $.post({
	// Target
	url: '/api/events',
	// Data we expect back
	dataType: 'json',
	// We will send json, so set the content type
	contentType: "application/json; charset=utf-8",
	// The data we send. JSON.stringify creates json from this object
	data: JSON.stringify({
	  gpio: Number.parseInt($('#evGPIO').val()),
	  state: $('#evState').is(':checked') ? 1 : 0,
	  time: Number.parseInt($('#evTime').val()),
	})
  }).done(addedEvent => {
	events.push(addedEvent);
	updateTable();
  }).fail(() => {
	alert('Adding the event failed.');
  });
}

// Deletes an event
function deleteEvent(eventid) {
  $.ajax({
	// Target
	url: '/api/events/'+eventid,
	// Data we expect back
	dataType: 'json',
	// We want a delete request
	type: 'DELETE',
  }).done(addedEvent => {
	// in-place filtering
	let tmpEvents = [];
	while(events.length > 0) {
	  let e = events.pop();
	  if (e.id !== eventid) tmpEvents.push(e);
	}
	tmpEvents.forEach(e => events.push(e));
	updateTable();
  }).fail(() => {
	alert('Adding the event failed.');
  });
}

// Re-renders the event table
function updateTable() {
  sortedEvents = events
	// Filter old events
	.filter(ev => ev.time >= uptime)
	// Sort by time
	.sort((a,b) => a.time===b.time ? 0 : (a.time < b.time ? -1 : 1));
  // Clear table
  $('#tbody').html("");
  // Rewrite table
  sortedEvents.forEach(ev => $('#tbody').append(
	'<tr><td>'+ev.time+'</td><td>GPIO'+ev.gpio+'</td><td>'+(ev.state===1?'HIGH':'LOW')+'</td>' +
	'<td><button type=\"button\" onClick=\"deleteEvent('+ev.id+');\">Delete</button></td></tr>'
  ));
}

// When the page is ready...
$(document).ready(() => {
  // Initially update uptime:
  updateUptime();

  // Initially poll the events
  updateEventlist();
});