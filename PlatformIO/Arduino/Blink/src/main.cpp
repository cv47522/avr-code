#include <Arduino.h>
// #define LED 13

// Declare each custom function before it will be called.
void showLED(int pin, int delayPeriod, bool ledStatus);


void setup()
{
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println("Start...");
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop()
{
  // put your main code here, to run repeatedly:
  showLED(LED_BUILTIN, 1000, HIGH);
  showLED(LED_BUILTIN, 500, LOW);
}

void showLED(int pin, int delayPeriod, bool ledStatus)
{
  if (ledStatus)
  {
    Serial.println("ON");
    digitalWrite(pin, HIGH);
  }
  else
  {
    Serial.println("OFF");
    digitalWrite(pin, LOW);
  }
  delay(delayPeriod);
}