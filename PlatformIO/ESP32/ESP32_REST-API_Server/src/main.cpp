#include <Arduino.h>
#include <WiFi.h>
#include <WebServer.h>
#include <ArduinoJson.h>
#include <analogWrite.h>

const char *SSID = "Aalto Studios";
const char *PWD = "makecoffeenotwar";

#define NUM_OF_LEDS 8
#define PIN 4
const int rows = 4;
const int columns = 5;

// v3: Left-Hand, M15: IO4???, M8 (PCB): IO13???
int motorsArr[rows][columns] = {
                                  {19, 26, 27, 14, 23}, // thumb~little
                                  {5, 13, 33, 12, 22},
                                  {21, 17, 16, 18, 4}, // dw1(thumb), dw2, up1, dw3, up2
                                  {32, 25, 15, -1, -1} // back1(pinky)~3
                                };


// Web server running on port 80
WebServer server(80);

// JSON data buffer
StaticJsonDocument<250> jsonDocument;
char buffer[250];

// env variable
long uptime;

/*---------------------------------------------*/
void connectToWiFi() {
  Serial.print("Connecting to ");
  Serial.println(SSID);

  WiFi.begin(SSID, PWD);

  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
    // we can even make the ESP32 to sleep
  }

  Serial.print("Connected. IP: ");
  Serial.println(WiFi.localIP());
}


void create_json(char *tag, float value, char *unit) {
  jsonDocument.clear();
  jsonDocument["type"] = tag;
  jsonDocument["value"] = value;
  jsonDocument["unit"] = unit;
  serializeJson(jsonDocument, buffer);
}

void add_json_object(char *tag, float value, char *unit) {
  JsonObject obj = jsonDocument.createNestedObject();
  obj["type"] = tag;
  obj["value"] = value;
  obj["unit"] = unit;
}

void read_system_data(void * parameter) {
   for (;;) {
     // Set the uptime key to the uptime in seconds
     uptime = millis()/1000;
    //  humidity = bme.readHumidity();
     Serial.println("Read system data");

     // delay the task
     vTaskDelay(10000 / portTICK_PERIOD_MS);
   }
}

void getUpTime() {
  Serial.println("Get uptime");
  // Set the uptime key to the uptime in seconds
  create_json("uptime", uptime, "seconds");
  server.send(200, "applicarion/json", buffer);
}

void handlePost() {
  if (server.hasArg("plain") == false) {
    //handle error here
  }
  String body = server.arg("plain");
  deserializeJson(jsonDocument, body);

  // Get RGB components
  int gpio = jsonDocument["gpio"];
  int intensity = jsonDocument["intensity"];
  int duration = jsonDocument["duration"];
  Serial.print("gpio: ");
  Serial.println(gpio);
  Serial.print("intensity: ");
  Serial.println(intensity);
  Serial.print("duration: ");
  Serial.println(duration);

  analogWrite(gpio, intensity);

  // Respond to the client
  server.send(200, "application/json", "{}");
}

void setup_task() {
  xTaskCreate(
  read_system_data,
  "Read system data",
  1000,
  NULL,
  1,
  NULL
  );
}


void setup_routing() {
  server.on("/api/uptime", getUpTime);
  server.on("/api/motor", HTTP_POST, handlePost);

  // start server
  server.begin();
}
/*---------------------------------------------*/
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);

  // Sensor setup
  // if (!bme.begin(0x76)) {
  //   Serial.println("Problem connecting to BME280");
  // }
  connectToWiFi();
  setup_task();
  setup_routing();
  // Initialize Neopixel
  // pixels.begin();

  // Set the pins that we will use as output pins
  for (int i=0; i<rows; i++) {
    for (int j=0; j<columns; j++) {
      pinMode(motorsArr[i][j], OUTPUT);
    }
  }
}

void loop() {
  // put your main code here, to run repeatedly:
  server.handleClient();
}