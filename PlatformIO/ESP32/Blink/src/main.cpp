#include <Arduino.h>
#include <ArduinoJson.h>
#include "variables.h"

// Declare each custom function before it will be called.
void showLED(int pin, int delayPeriod, bool ledStatus);


void setup()
{
  // put your setup code here, to run once:
  Serial.begin(115200);
  Serial.println("Start...");
  pinMode(LED_BUILTIN, OUTPUT); // ESP32 Feather LED_BUILTIN: 13
}

void loop()
{
  // put your main code here, to run repeatedly:
  showLED(LED_BUILTIN, 1000, HIGH);
  showLED(LED_BUILTIN, 200, LOW);

  print_global_x();
}

void showLED(int pin, int delayPeriod, bool ledStatus)
{
  if (ledStatus)
  {
    Serial.println("ON");
    digitalWrite(pin, HIGH);
  }
  else
  {
    Serial.println("OFF");
    digitalWrite(pin, LOW);
  }
  delay(delayPeriod);
}