#ifndef VARIABLES_H_
#define VARIABLES_H_

// declare global var
// any source file that includes this
// will be able to use "global_x"
extern int global_x;

void print_global_x();
#endif