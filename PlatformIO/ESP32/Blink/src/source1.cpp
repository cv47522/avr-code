#include <Arduino.h>
#include "variables.h"

int global_x;
void print_global_x()
{
    // since global_x still needs to be defined somewhere,
    // we define it (for example) in this source file
    //set global_x here:
    global_x = 5;

    Serial.println(global_x);
    delay(500);
}
