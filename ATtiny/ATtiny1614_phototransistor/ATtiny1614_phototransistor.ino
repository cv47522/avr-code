#include <avr/io.h>
#include <util/delay.h>
#include <SoftwareSerial.h>
//Check out pins from ~/Library/Arduino15/packages/megaTinyCore/hardware/megaavr/2.0.1/variants/txy4/pins_arduino.h
#define RX    9   // *** PA2
#define TX    8   // *** PA1

SoftwareSerial mySerial(RX, TX);

#define LED_AVR PIN4_bm //PA4:LED pin
#define BUTTON_AVR PIN5_bm 
#define PHOTO_AVR PIN7_bm 

void setup() {
/** Setup Instruction
  PORTA.DIRSET = PIN4_bm; // use PA4 as an output
  PORTA.DIRCLR = PIN4_bm; // use PA4 as an input
  or
  PORTA.OUTSET = PIN4_bm; // turn PA4 output on
  PORTA.OUTCLR = PIN4_bm; // turn PA4 output off
  or
  PORTA.OUTTGL = PIN4_bm; // toggle PA4 output
**/
   mySerial.begin(115200);
   PORTA.DIRSET = LED_AVR; //Output
   PORTA.DIRCLR = BUTTON_AVR; // Input
   PORTA.DIRCLR = PHOTO_AVR; // Input
    
   PORTA.PIN5CTRL = PORT_PULLUPEN_bm; // use the internal pullup resistor on PA5
}

void loop() {
/** HIGH/LOW Instruction 
  PORTA.OUT |= PIN4_bm; // write PA4 high
  PORTA.OUT &= ~PIN4_bm; // write PA4 low
**/

  bool state = PORTA.IN & PHOTO_AVR; // read the state of a pin
  
  switch(state) {
    case 1:
    PORTA.OUT &= ~LED_AVR;
    break;
    case 0:
    PORTA.OUT |= LED_AVR;
    break;
    default:
    PORTA.OUT &= ~LED_AVR;
    break;
  }
  mySerial.println(state);
  delay(200);
 }
