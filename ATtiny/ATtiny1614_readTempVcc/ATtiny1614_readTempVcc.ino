/* Minimal demo of uaing the ADC to read temperature and operating voltage */
#include <SoftwaremySerial.h>
//Check out pins from ~/Library/Arduino15/packages/megaTinyCore/hardware/megaavr/2.0.1/variants/txy4/pins_arduino.h
#define RX    9   // *** PA2
#define TX    8   // *** PA1

SoftwareSerial mySerial(RX, TX);
#define RESULTCOUNT 4
int16_t results[RESULTCOUNT];
int32_t sum;
int16_t average;

void setup() {
  // put your setup code here, to run once:
  delay(1000);
  mySerial.begin(57600);
}

uint16_t readSupplyVoltage() { //returns value in millivolts to avoid floating point
  analogReference(VDD);
  VREF.CTRLA = VREF_ADC0REFSEL_1V5_gc; /* Voltage reference at 1.5V */
  uint16_t reading = analogRead(ADC_INTREF);
  uint32_t intermediate = 1023*1500;
  reading=intermediate/reading;
  return reading;
}
void printRegisters(){
  mySerial.print("ADC0.MUXPOS: ");
  showHex(ADC0.MUXPOS);
  mySerial.print("  ADC0.CTRLC: ");
  showHex(ADC0.CTRLC);
  mySerial.print("  VREF.CTRLA: ");
  showHex(VREF.CTRLA);
  mySerial.println();
}

uint16_t readTemp() {
  //based on the datasheet, in section 30.3.2.5 Temperature Measurement
  int8_t sigrow_offset = SIGROW.TEMPSENSE1; // Read signed value from signature row
  uint8_t sigrow_gain = SIGROW.TEMPSENSE0; // Read unsigned value from signature row
  analogReference(INTERNAL1V1);
  ADC0.SAMPCTRL = 0x1F; //Appears very necessary!
  ADC0.CTRLD |= ADC_INITDLY_DLY32_gc; //Doesn't seem so necessary?
  uint16_t adc_reading = analogRead(ADC_TEMPERATURE); // ADC conversion result with 1.1 V internal reference
  mySerial.println(adc_reading);
  analogReference(VDD);
  ADC0.SAMPCTRL = 0x0;
  ADC0.CTRLD &= ~(ADC_INITDLY_gm);
  uint32_t temp = adc_reading - sigrow_offset;
  mySerial.println(temp);
  temp *= sigrow_gain; // Result might overflow 16 bit variable (10bit+8bit)
  mySerial.println(temp);
  temp += 0x80; // Add 1/2 to get correct rounding on division below
  temp >>= 8; // Divide result to get Kelvin
  return temp;
}

void showHex (const byte b) {
  char buf [3] = { ((b >> 4) & 0x0F) | '0', (b & 0x0F) | '0', 0};
  if (buf [0] > '9')
    buf [0] += 7;
  if (buf [1] > '9')
    buf [1] += 7;
  mySerial.print(buf);
}
void loop() {
  int16_t reading=readSupplyVoltage();
  mySerial.print("System voltage is: ");
  mySerial.print(reading);
  mySerial.println(" mV");
  reading = readTemp();
  mySerial.print("System temperature is: ");
  mySerial.print(reading);
  mySerial.println(" K");

  delay(10000);
}
