#include <avr/io.h>
#include <util/delay.h>
#include <SoftwareSerial.h>
//Check out pins from ~/Library/Arduino15/packages/megaTinyCore/hardware/megaavr/2.0.1/variants/txy4/pins_arduino.h
#define RX    9   // *** PA2
#define TX    8   // *** PA1

SoftwareSerial mySerial(RX, TX);

#define LED_AVR PIN4_bm //PA4:LED pin
#define BUTTON_AVR PIN5_bm 
#define PHOTO_AVR PIN7_bm 
#define MIC_AVR PIN6_bm 

// Variables to find the peak-to-peak amplitude of AUD output
const int sampleTime = 50; 
int micOut;

//previous VU(volume unit) value
int preValue = 0; 


void setup() {
   mySerial.begin(115200);
   PORTA.DIRSET = LED_AVR; //Output
   PORTA.DIRCLR = BUTTON_AVR; // Input
   PORTA.DIRCLR = PHOTO_AVR; // Input
   PORTA.DIRCLR = MIC_AVR; // Input
    
   PORTA.PIN5CTRL = PORT_PULLUPEN_bm; // use the internal pullup resistor on PA5
}

void loop() {
//  int micOutput = findPTPAmp();
//   VUMeter(micOutput);   

  int state = PORTA.IN & MIC_AVR; // read the state of a pin
  
  if(state > 0) {
   PORTA.OUT |= LED_AVR;
  }
  else {
   PORTA.OUT &= ~LED_AVR;
  }
  
  bool btn_state = PORTA.IN & BUTTON_AVR; // read the state of a pin
  switch(btn_state) {
    case 1:
    PORTA.OUT &= ~LED_AVR; // off
    break;
    case 0:
    PORTA.OUT |= LED_AVR; //on
    break;
    default:
    PORTA.OUT &= ~LED_AVR;
    break;
  }
  mySerial.println(state);
  delayMicroseconds(10);
 }



 // Find the Peak-to-Peak Amplitude Function
int findPTPAmp(){
// Time variables to find the peak-to-peak amplitude
   unsigned long startTime = millis();  // Start of sample window
   unsigned int PTPAmp = 0; 

// Signal variables to find the peak-to-peak amplitude
   unsigned int maxAmp = 0;
   unsigned int minAmp = 1023;

// Find the max and min of the mic output within the 50 ms timeframe
   while(millis() - startTime < sampleTime) 
   {

      micOut = PORTA.IN & MIC_AVR;

      if( micOut < 1023) //prevent erroneous readings
      {
        if (micOut > maxAmp)
        {
          maxAmp = micOut; //save only the max reading
        }
        else if (micOut < minAmp)
        {
          minAmp = micOut; //save only the min reading
        }
      }
   }

  PTPAmp = maxAmp - minAmp; // (max amp) - (min amp) = peak-to-peak amplitude
  double micOut_Volts = (PTPAmp * 3.3) / 1024; // Convert ADC into voltage

  //Uncomment this line for help debugging (be sure to also comment out the VUMeter function)
  //Serial.println(PTPAmp); 

  //Return the PTP amplitude to use in the soundLevel function. 
  // You can also return the micOut_Volts if you prefer to use the voltage level.
  return PTPAmp;   
}


// Volume Unit Meter function: map the PTP amplitude to a volume unit between 0 and 10.
int VUMeter(int micAmp){

  // Map the mic peak-to-peak amplitude to a volume unit between 0 and 10.
   // Amplitude is used instead of voltage to give a larger (and more accurate) range for the map function.
   // This is just one way to do this -- test out different approaches!
  int fill = map(micAmp, 23, 750, 0, 10); 

  // Only print the volume unit value if it changes from previous value
  while(fill != preValue)
  {
    mySerial.println(fill);
    preValue = fill;
  }
}
