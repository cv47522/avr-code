#include <avr/io.h>

#define LED_AVR PIN3_bm //PA3: LED pin

void setup() {
   PORTA.DIRSET = LED_AVR;
}

void loop() {
  PORTA.OUT |= LED_AVR;
  delay(100);
  PORTA.OUT &= ~LED_AVR;
  delay(100);
 }
