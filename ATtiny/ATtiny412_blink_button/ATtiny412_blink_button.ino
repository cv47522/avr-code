#include <avr/io.h>

#define LED_AVR PIN3_bm //PA3: LED pin
#define BUTTON_AVR PIN6_bm //PA6: Button pin

void setup() {
/** Setup Instruction
  PORTA.DIRSET = PIN4_bm; // use PA4 as an output
  PORTA.DIRCLR = PIN4_bm; // use PA4 as an input
  or
  PORTA.OUTSET = PIN4_bm; // turn PA4 output on
  PORTA.OUTCLR = PIN4_bm; // turn PA4 output off
  or
  PORTA.OUTTGL = PIN4_bm; // toggle PA4 output
**/
   PORTA.DIRSET = LED_AVR;
   PORTA.DIRCLR = BUTTON_AVR;
}

void loop() {
/** HIGH/LOW Instruction 
  PORTA.OUT |= PIN4_bm; // write PA4 high
  PORTA.OUT &= ~PIN4_bm; // write PA4 low
**/
  bool state = PORTA.IN & BUTTON_AVR; // read the state of a pin
  switch(state) {
    case 0:
    PORTA.OUT &= ~LED_AVR;
    break;
    case 1:
    PORTA.OUT |= LED_AVR;
    break;
    default:
    PORTA.OUT &= ~LED_AVR;
    break;
  }
 }
