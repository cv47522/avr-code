//
// hello.t1614.echo.ino
//
// tiny1614 echo hello-world
//    115200 baud

#include <SoftwareSerial.h>
//Check out pins from ~/Library/Arduino15/packages/megaTinyCore/hardware/megaavr/2.0.1/variants/txy4/pins_arduino.h
#define RX    9   // *** PA2
#define TX    8   // *** PA1

SoftwareSerial mySerial(RX, TX);

#define max_buffer 25

static int index = 0;
static char chr;
static char buffer[max_buffer] = {0};

void setup() {
   mySerial.begin(115200);
   }

void loop() {
   if (mySerial.available() > 0) {
      chr = mySerial.read();
      mySerial.print("hello.t1614.echo: you typed \"");
      buffer[index++] = chr;
      if (index == (max_buffer-1))
         index = 0;
      mySerial.print(buffer);
      mySerial.println("\"");
      }
   }
