//
//  hello.412.blink.c Syntax
//
//
//  Created by Hsieh Wan Ting on 2020/3/25.
//

#include <avr/io.h>
#include <util/delay.h>

int main(void) {
    PORTA.DIRSET = PIN3_bm;// or PORTA.DIR = 0b01000000; // Use LED: PA3 as Output
    PORTA.DIRCLR = PIN6_bm;// Use Button: PA6 as Input

    while (1) {
        // int btn_state = PORTA.IN & PIN6_bm; // Read state from Button & Generate 1 bit

        // if (btn_state == 1) {
        //     PORTA.OUT |= PIN3_bm; // Write LED HIGH
        // } else {
        //     PORTA.OUT &= ~PIN3_bm; // Write LED LOW
        // }
        PORTA.OUT |= LED_AVR;
        _delay_ms(500);
        PORTA.OUT &= ~LED_AVR;
        _delay_ms(500);
    }
}
