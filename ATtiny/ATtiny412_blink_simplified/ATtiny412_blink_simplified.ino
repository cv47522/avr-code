#define LED_AVR 4 //PA3: LED pin

void setup() {
   pinMode(LED_AVR, OUTPUT);
}

void loop() {
  digitalWrite(LED_AVR, HIGH);
  delay(100);
  digitalWrite(LED_AVR, LOW);
  delay(100);
 }
