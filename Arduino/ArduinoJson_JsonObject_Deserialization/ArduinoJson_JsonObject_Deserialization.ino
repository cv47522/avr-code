#include <ArduinoJson.h>

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);

  const size_t CAPACITY = JSON_OBJECT_SIZE(1); // w/ 1 key-value pair
  
  // Allocate the JSON document, Stack: stack, Dynamic: heap
  StaticJsonDocument<CAPACITY> doc1;
  StaticJsonDocument<CAPACITY> doc2;
  
  // create an object
  JsonObject obj1 = doc1.to<JsonObject>();
  obj1["hello"] = "world";

  // Serialize the object and send the result to Serial
  serializeJson(doc1, Serial);

  
  // JSON input string.
  char json[] = "{\"sensor\":\"gps\"}";

  // Deserialize the JSON document
  DeserializationError error = deserializeJson(doc2, json);

  // Test if parsing succeeds.
  if (error) {
    Serial.print(F("desrializeJson() failed: "));
    Serial.println(error.f_str());
    return;
  }
  
  // extract the data
  JsonObject obj2 = doc2.as<JsonObject>();
  const char* sensor = obj2["sensor"];

   // Print values.
   Serial.println(sensor);
}

void loop() {
  // put your main code here, to run repeatedly:

}
