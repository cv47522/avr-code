#define MOTOR_STEPS 200

#define DIR 13 //PC0
#define STEP 12 //PC1
//#define SLEEP 11 //PC2
//#define RESET 10 //PC3
//#define MS1 3 //PA2
//#define MS2 4 //PA1
#define ENABLE 2 //PA3



void setup() {
  pinMode(DIR, OUTPUT);
  pinMode(STEP, OUTPUT);
//  pinMode(SLEEP, OUTPUT);
//  pinMode(RESET, OUTPUT);
//  pinMode(MS1, OUTPUT);
//  pinMode(MS2, OUTPUT);
  pinMode(ENABLE, OUTPUT);


  /* Full-Step mode */
//  digitalWrite(MS1, LOW);
//  digitalWrite(MS2, LOW);
  
  /* 
   SLEEP(LOW): sleep mode -> minimizing the power consumption, 
   RESET(HIGH): enable the driver,
   ENABLE(LOW): the driver is activated
  */
  digitalWrite(ENABLE, LOW);
//  digitalWrite(SLEEP, HIGH);
//  digitalWrite(RESET, HIGH);
  
  
  
}

void loop() {
  // Set the spinning direction clockwise:
  digitalWrite(DIR, HIGH);

  for (int i = 0; i < MOTOR_STEPS; i++) {
    // These four lines result in 1 step:
    digitalWrite(STEP, HIGH);
    delay(1);
    
    digitalWrite(STEP, LOW);
    delay(1);
  }
  delay(1);


}
