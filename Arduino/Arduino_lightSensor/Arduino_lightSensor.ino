int light;
int pot;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  light = analogRead(A0);
  pot = analogRead(A1);
  Serial.print(light);
  Serial.print(", ");
  Serial.print(pot);
  Serial.println();
  delay(1);
}
