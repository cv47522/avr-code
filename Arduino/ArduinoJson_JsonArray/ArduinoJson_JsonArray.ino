#include <ArduinoJson.h>

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);

  const size_t CAPACITY = JSON_OBJECT_SIZE(3); // 48 bytes
  
  // Allocate the JSON document
  // Inside the brackets, 200 is the RAM allocated to this document.
  // Don't forget to change this value to match your requirement.
  // Use arduinojson.org/v6/assistant to compute the capacity.
  // Here: doc needs 64 bytes to store 3 key-value pairs
  StaticJsonDocument<200> doc;
  
  // Add values in the document
  doc["sensor"] = "gps";
  doc["time"] = 1351824120;

  // Add an array.
  JsonArray dataArr = doc.createNestedArray("data");
  dataArr.add(48.1645613);
  dataArr.add(2.46431);

  // Generate the minified JSON and send it to the Serial port.
  serializeJson(doc, Serial); // output: {"sensor":"gps","time":1351824120,"data":[48.756080,2.302038]}

  // Start a new line
  Serial.println();
  Serial.print("Capacity(bytes): ");
  Serial.println(CAPACITY);

  // Data types checking
  Serial.println(doc["time"].is<long>()); // 1 true
  Serial.println(doc["time"].is<bool>()); // 0 false
  Serial.println(doc["data"].is<JsonArray>()); // 1 true
  
  // Generate the prettified JSON and send it to the Serial port.
  serializeJsonPretty(doc, Serial);
  // output:
  // {
  //   "sensor": "gps",
  //   "time": 1351824120,
  //   "data": [
  //     48.756080,
  //     2.302038
  //   ]
  // }
}

void loop() {
  // put your main code here, to run repeatedly:

}
