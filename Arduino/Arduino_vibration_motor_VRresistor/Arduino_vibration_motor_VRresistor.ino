int vmotor = 13; // PWM - Mirco: 13
int VRamplitude = A0;
int VRdelay = A1;
int VRduration = A2;

unsigned int previousTime = 0; // max. millis(): 4 billion (ms) = 49 days

void setup() {
  // put your setup code here, to run once:
  pinMode(vmotor, OUTPUT);
  pinMode(VRamplitude, INPUT);
  pinMode(VRdelay, INPUT);
  Serial.begin(9600);
}

void loop() {
  unsigned int currentTime = millis(); // max. millis(): 4 billion (ms) = 49 days
  
  // put your main code here, to run repeatedly:
  int motorAmplitude = analogRead(VRamplitude);
  motorAmplitude = map(motorAmplitude, 0, 1023, 0, 255);
  Serial.print("Amplitude: ");
  Serial.print(motorAmplitude);
  
  int motorInterval = analogRead(VRdelay);
  motorInterval = map(motorInterval, 0, 1023, 10, 5000);
  Serial.print(", Interval(ms): ");
  Serial.print(motorInterval);

  if (currentTime - previousTime >= motorInterval){
    analogWrite(vmotor, motorAmplitude); // turn ON
    previousTime = currentTime;
  }
  else{
    analogWrite(vmotor, 0); // turn OFF
  }

  int motorDuration = analogRead(VRduration);
  motorDuration = map(motorDuration, 0, 1023, 10, 3000);
  Serial.print(", Duration(ms): ");
  Serial.println(motorDuration);
  
  delay(motorDuration);
}
