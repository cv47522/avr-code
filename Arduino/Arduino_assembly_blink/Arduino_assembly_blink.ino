#include <avr/io.h>
#include <util/delay.h>

const int btn_pin = 11; // PB3
const int led_pin = 10; // PB2

void setup() {
//  pinMode(led_pin, OUTPUT);

  DDRB = B00000100; // Set LED: PB2 as Output
  PORTB = B00001000; // Pull up Button: PB3 resistor & LOW LED
  Serial.begin(9600);
}

void loop() {
//  digitalWrite(led_pin, LOW);
//  delay(500);
//  digitalWrite(led_pin, HIGH);
//  delay(500);

//  PORTD = B00000100;
//  _delay_ms(200);
//  PORTD = B00000000;
//  _delay_ms(200);

//  int btn_state = digitalRead(btn_pin);

  int btn_state = ((1 << 3) & PINB) >> 3; // Generate 1 bit
 
  if (btn_state == LOW) {
    PORTB = (1 << 2) | PORTB;
    Serial.println(btn_state);
  } else {
    PORTB = ~ (1 << 2) & PORTB;
    Serial.println(btn_state);
  }
  
}
