#include <Morse.h>

Morse morse(13);

void setup() {
  // put your setup code here, to run once:

}

void loop() {
  morse.dot(); morse.dot(); morse.dot();
  morse.dash(); morse.dash(); morse.dash();
  morse.dot(); morse.dot(); morse.dot();
  delay(3000);
}

//int LED = 13;
//void setup() {
//  // put your setup code here, to run once:
//  pinMode(LED, OUTPUT);
//
//}
//void loop() {
//  // put your main code here, to run repeatedly:
//  dot(); dot(); dot(); // S
//  dash(); dash(); dash(); // O
//  dot(); dot(); dot(); // S
//  delay(3000);
//
//}
//
//void dot() {
//  digitalWrite(LED, HIGH);
//  delay(250);
//  digitalWrite(LED, LOW);
//  delay(250);
//}
//
//void dash() {
//  digitalWrite(LED, HIGH);
//  delay(1000);
//  digitalWrite(LED, LOW);
//  delay(250);
//}
