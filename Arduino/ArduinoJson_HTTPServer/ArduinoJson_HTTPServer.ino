#include <ArduinoJson.h>

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);

  // 96 bytes
  char json[] = "[{\"gpio\":27,\"state\":1,\"time\":42,\"id\":0},{\"gpio\":2,\"state\":0,\"time\":102,\"id\":1}]";
  
  // Allocate the JSON document
  StaticJsonDocument<200> doc;

  // parse a JSON array
  DeserializationError error = deserializeJson(doc, json);

  // Test if parsing succeeds.
  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.f_str());
    return;
  }

  // extract the values
  JsonArray arr = doc.as<JsonArray>();
  for(JsonVariant v : arr) {
    Serial.println(v.as<int>());
  }
}

void loop() {
  // put your main code here, to run repeatedly:

}
