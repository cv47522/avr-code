// y(t) = A sin(wt + p)
// where: A = amplitude, w = angular frequency, t = time, p = phase
// 360 degrees == 2*PI rad

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);

}

void loop() {
  // put your main code here, to run repeatedly:
 // randomArray[random(0,3)];
  for (int t=0; t<360;t++) {
//    float y1 = 1 * sin((PI/180 * t) + 0);
    float y1 = sineVariable(1, 1, t, 0);
    float y1_plus = sineVariable(1, 1, t, 0) + random(-1.0, 1.0); // still too regular
    // random value for intensity
    // replace t w/ dynamic variable
    
//    float y2 = 3 * sin((2*PI/180 * t) + 45);
    float y2 = sineVariable(3, 2, t, 45);
    
//    float y3 = 6 * sin((5*PI/180 * t) + 180);
    float y3 = sineVariable(6, 5, t, 180);

    Serial.print(y1);
    Serial.print(" ");
    Serial.print(y1_plus);
    Serial.print(" ");
    Serial.print(y2);
    Serial.print(" ");
    Serial.print(y3);
    Serial.println(" ");
    delay(50);
  }

}

float sineVariable(int amplitude, int degree, int t, int phase) {
  // ampl: intensity, t: control interval bewteen 0 and 1, random No. between 0 and 1
  return amplitude * sin((degree*PI/180 * t) + phase);
}
