# Codes for AVR Boards
Including: (Board type - IDE)
- Arduino - Arduino IDE
- ATtiny - Arduino IDE (related circuit repositories can be found [here](https://gitlab.com/users/cv47522/projects))
- ESP32 - Arduino IDE
- Teensy 4.0 - Arduino IDE
- Bela board - Pure Data